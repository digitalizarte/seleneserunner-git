namespace SeleneseGridRunner
{
    using System;

    static class ConsoleExtension
    {
        public static void ColorWriteLine(ConsoleColor color, string format, params object[] args)
        {
            ConsoleColor actual = Console.ForegroundColor;
            Console.ForegroundColor = color;
            Console.WriteLine(format, args);
            Console.ForegroundColor = actual;
        }

        public static void ColorWrite(ConsoleColor color, string format, params object[] args)
        {
            ConsoleColor actual = Console.ForegroundColor;
            Console.ForegroundColor = color;
            Console.Write(format, args);
            Console.ForegroundColor = actual;
        }
    }
}
