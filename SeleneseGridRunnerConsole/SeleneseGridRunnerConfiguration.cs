﻿namespace SeleneseGridRunner
{
    using System.Configuration;

    internal static class SeleneseGridRunnerConfiguration
    {
        public static string BaseUrl
        {
            get
            {
                return ConfigurationSettings.AppSettings["BaseUrl"];
            }
        }

        public static string HubUrl
        {
            get
            {
                return ConfigurationSettings.AppSettings["HubUrl"];
            }
        }

        public static string HtmlSuites
        {
            get
            {
                return ConfigurationSettings.AppSettings["HtmlSuites"];
            }
        }

        public static bool IsDebug
        {
            get
            {
                bool ret;
                if (!bool.TryParse(ConfigurationSettings.AppSettings["IsDebug"], out ret))
                {
                    ret = false;
                }
                return ret;
            }
        }

        public static string Browsers
        {
            get
            {
                return ConfigurationSettings.AppSettings["Browsers"];
            }
        }
    }
}
