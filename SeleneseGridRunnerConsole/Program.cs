﻿namespace SeleneseGridRunner
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using ConsoleLib;
    using NUnit.Framework;

    class Program : ProgramBase
    {

        int processReturn = 1;

        static int Main(string[] args)
        {
            Program prg = new Program(args);
            prg.ConfigOptions.IgnoreZeroArguments = true;
            prg.ConfigOptions.PauseOnEnd = false;
            prg.ConfigOptions.LaunchBrowserOnError = prg.IsDebug();
            prg.ConfigOptions.LogExceptions = false;
            prg.Init();

            return prg.processReturn;
        }

        public Program(string[] args)
            : base(args)
        {
            InitArguments();
        }

        public Program(Arguments arguments)
            : base(arguments)
        {
            InitArguments();
        }

        private void InitArguments()
        {
            argumentsHelp["hub"] = "Url del servidor de Selenium Grid.";
            argumentsHelp["htmlSuite"] = "Ruta de la suite HTML a ejecutar.";
            argumentsHelp["browsers"] = "Lista de navegadores separada por coma.";
            argumentsHelp["baseUrl"] = "Opcional. Url base para los test, si no se especifica se utiliza la que esta definida en cada TestCase.";
            argumentsHelp["htmlSuites"] = "Opcional. Lista de las rutas de los archivos HTML a ejecutar.";
            argumentsHelp["tests"] = "Opcional. Lista de las rutas de los archivos HTML a ejecutar.";
            argumentsHelp["t"] = "Opcional. Lista de las rutas de los archivos HTML a ejecutar.";
            argumentsHelp["l"] = "Opcional. Ejecuta los TestCase localmente.";
            argumentsHelp["local"] = "Opcional. Ejecuta los TestCase localmente.";
            argumentsHelp["d"] = "Opcional. Muestra los mensajes de debug.";
            argumentsHelp["debug"] = "Opcional. Muestra los mensajes de debug.";
            argumentsHelp["takeSS"] = "Opcional. Toma una foto del final de la prueba.";
            argumentsHelp["takeSSAll"] = "Opcional. Toma una foto de cada pasa de la prueba.";
            argumentsHelp["takeSSDir"] = "Opcional. Directorio para almacenar las fotos de la prueba.";
            argumentsHelp["s"] = "Opcional. Velocidad de ejecución de los comandos.";
            argumentsHelp["speed"] = "Opcional. Velocidad de ejecución de los comandos.";
            argumentsHelp["sv"] = "Opcional. Comparte las variables entre todos los TestCase de la suite.";
            argumentsHelp["sharevars"] = "Opcional. Comparte las variables entre todos los TestCase de la suite.";
        }

        protected override void Process()
        {
            CheckArguments();

            string baseUrl = arguments["baseUrl"] ?? SeleneseGridRunnerConfiguration.BaseUrl,
                   hubUrl = arguments["hub"] ?? SeleneseGridRunnerConfiguration.HubUrl;
            int speed;

            IList<TestSuite> suites = HtmlHelper.Parse(HtmlSuites());
            IList<string> browsers = Browsers();
            SelenseExecutorConfiguration config = new SelenseExecutorConfiguration(suites, browsers, baseUrl, hubUrl);
            bool takeSS, takeSSAll, debug, local, shareVars;
            if (!bool.TryParse(arguments["takeSS"], out takeSS))
            {
                takeSS = false;
            }

            if (!bool.TryParse(arguments["takeSSAll"], out takeSSAll))
            {
                takeSSAll = false;
            }

            if (!bool.TryParse(arguments["d"] ?? arguments["debug"], out debug))
            {
                debug = false;
            }

            if (!int.TryParse(arguments["s"] ?? arguments["speed"], out speed))
            {
                speed = 0;
            }

            if (!bool.TryParse(arguments["l"] ?? arguments["local"], out local))
            {
                local = false;
            }

            if (!bool.TryParse(arguments["sv"] ?? arguments["sharevars"], out shareVars))
            {
                shareVars = false;
            }

            config.Speed = speed;
            config.TakeScreenShot = takeSS;
            config.TakeScreenShotAll = takeSSAll;
            config.TakeScreenShotDirectory = arguments["takeSSDir"];
            config.IsDebug = debug;
            config.IsLocal = local;
            config.UseSharedVars = shareVars;
            if (debug)
            {
                config.Out = Console.Out;
            }

            // IList<TestSuiteResult> results = SeleneseExecutor.Execute(suites, browsers, baseUrl, hubUrl);
            IList<TestSuiteResult> results = SeleneseExecutorHelper.Execute(config);
            ShowResults(results);
            
            processReturn = 0;
        }

        private void ShowResults(IList<TestSuiteResult> results)
        {
            ArrayPrinter printer = new ArrayPrinter();
            ConsoleColor color;
            Console.Title = "Test results";
            const string format = "{0}\n";
            foreach (TestSuiteResult suite in results)
            {
                Console.WriteLine("Test suite results");
                Console.WriteLine(suite.FilePath);
                string testStatus = Enum.GetName(typeof(TestStatus), suite.Status);
                if (suite.Status == TestStatus.Passed)
                {
                    ConsoleExtension.ColorWriteLine(ConsoleColor.DarkGreen, "Result: {0}", testStatus);
                }
                else if (suite.Status == TestStatus.Failed)
                {
                    ConsoleExtension.ColorWriteLine(ConsoleColor.DarkRed, "Result: {0}", testStatus);
                }
                else
                {
                    Console.WriteLine("result: {0}", testStatus);
                }

                bool hasTestCaseResults = suite.TestCaseResults != null;
                Console.WriteLine("totalTime: {0}s", suite.Elapsed.TotalSeconds);
                if (hasTestCaseResults)
                {
                    Console.WriteLine("numTestTotal: {0}", suite.TestCaseResults.Count);
                    Console.Write("numTestPasses: "); ConsoleExtension.ColorWrite(ConsoleColor.DarkGreen, format, suite.TestCaseResults.Count(t => t.Status == TestStatus.Passed));
                    Console.Write("numTestFailures: "); ConsoleExtension.ColorWrite(ConsoleColor.DarkRed, format, suite.TestCaseResults.Count(t => t.Status == TestStatus.Failed));
                    Console.Write("numTestErrors: "); ConsoleExtension.ColorWrite(ConsoleColor.Red, format, suite.TestCaseResults.Count(t => t.Status == TestStatus.Inconclusive));

                    IEnumerable<TestCommandResult> query = from tcase in suite.TestCaseResults
                                                           from tcmd in tcase.TestCommandResults
                                                           select tcmd;
                    Console.Write("numCommandPasses: "); ConsoleExtension.ColorWrite(ConsoleColor.DarkGreen, format, query.Count(t => t.Status == TestStatus.Passed));
                    Console.Write("numCommandFailures: "); ConsoleExtension.ColorWrite(ConsoleColor.DarkRed, format, query.Count(t => t.Status == TestStatus.Failed));
                    Console.Write("numCommandErrors: "); ConsoleExtension.ColorWrite(ConsoleColor.Red, format, query.Count(t => t.Status == TestStatus.Inconclusive));
                    Console.Write("numCommandNoExecute: "); ConsoleExtension.ColorWrite(ConsoleColor.DarkGray, format, query.Count(t => t.Status == TestStatus.Skipped));
                    Console.WriteLine();
                    Console.WriteLine();
                    foreach (TestCaseResult testCase in suite.TestCaseResults)
                    {
                        if (testCase.Status == TestStatus.Passed)
                        {
                            color = ConsoleColor.DarkGreen;
                        }
                        else if (testCase.Status == TestStatus.Failed)
                        {
                            color = ConsoleColor.DarkRed;
                        }
                        else if (testCase.Status == TestStatus.Inconclusive)
                        {
                            color = ConsoleColor.Red;
                        }
                        else
                        {
                            color = Console.ForegroundColor;
                        }

                        ConsoleExtension.ColorWriteLine(color, "{0} ({1})", testCase.Name, Enum.GetName(typeof(TestStatus), testCase.Status));
                        ConsoleExtension.ColorWriteLine(color, "{0}", testCase.FilePath);
                        ConsoleExtension.ColorWriteLine(color, "{0}", testCase.BaseUrl);
                        ConsoleExtension.ColorWriteLine(color, "totalTime: {0}s", testCase.Elapsed.TotalSeconds);
                        foreach (TestCommandResult item in testCase.TestCommandResults.Where(tc => tc.Exception != null))
                        {
                            ConsoleExtension.ColorWriteLine(color, "{0}", item.Exception.Message);
                        }

                        int rows = testCase.TestCommandResults.Count + 1, i = 0;
                        string[,] data = new string[rows, 6];
                        ConsoleColor[,] colors = new ConsoleColor[rows, 6];
                        colors[i, 0] = color;
                        colors[i, 1] = color;
                        colors[i, 2] = color;
                        colors[i, 3] = color;
                        colors[i, 4] = color;
                        colors[i, 5] = color;
                        data[i, 0] = "Step";
                        data[i, 1] = "Command";
                        data[i, 2] = "Argument1";
                        data[i, 3] = "Argument2";
                        data[i, 4] = "Status";
                        data[i, 5] = "Duration";
                        i++;

                        foreach (TestCommandResult testCommand in testCase.TestCommandResults)
                        {
                            if (testCommand.Status == TestStatus.Passed)
                            {
                                color = ConsoleColor.DarkGreen;
                            }
                            else if (testCommand.Status == TestStatus.Failed)
                            {
                                color = ConsoleColor.DarkRed;
                            }
                            else if (testCommand.Status == TestStatus.Inconclusive)
                            {
                                color = ConsoleColor.Red;
                            }
                            else
                            {
                                color = Console.ForegroundColor;
                            }

                            colors[i, 0] = color;
                            colors[i, 1] = color;
                            colors[i, 2] = color;
                            colors[i, 3] = color;
                            colors[i, 4] = color;
                            colors[i, 5] = color;

                            data[i, 0] = i.ToString().PadLeft(5, '0');
                            data[i, 1] = testCommand.Command;
                            data[i, 2] = testCommand.Argument1;
                            data[i, 3] = testCommand.Argument2;
                            data[i, 4] = Enum.GetName(typeof(TestStatus), testCommand.Status);
                            data[i, 5] = String.Format("{0}ms", testCommand.Elapsed.TotalMilliseconds);
                            i++;
                        }

                        printer.PrintToConsole(data, colors);

                        Console.WriteLine();
                        Console.WriteLine();
                    }
                }
                else
                {
                    if (suite.Exception != null)
                    {
                        ConsoleExtension.ColorWrite(ConsoleColor.Red, "Exception: {0}", suite.Exception);
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void CheckArguments()
        {
            if (String.IsNullOrWhiteSpace(arguments["t"])
             && String.IsNullOrWhiteSpace(arguments["test"])
             && String.IsNullOrWhiteSpace(arguments["htmlSuite"])
             && String.IsNullOrWhiteSpace(arguments["htmlSuites"] ?? SeleneseGridRunnerConfiguration.HtmlSuites))
            {
                // throw new ArgumentException("Se espera el parametro htmlSuite con la ruta absoluta de la suite a testear.", "htmlSuite");
                throw new ArgumentException("No se espeficico ningun test o suite para ejecutar.");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool IsDebug()
        {
            return !String.IsNullOrWhiteSpace(arguments["debug"] ?? arguments["d"]) || SeleneseGridRunnerConfiguration.IsDebug;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IList<string> Browsers()
        {
            List<string> list = new List<string>();
            foreach (var item in arguments.Keys)
            {
                string key = (item as string);
                if (key.StartsWith("*", StringComparison.InvariantCultureIgnoreCase))
                {
                    list.Add(key);
                }
            }

            string browsers = arguments["browsers"] ?? SeleneseGridRunnerConfiguration.Browsers;
            if (!String.IsNullOrWhiteSpace(browsers))
            {
                string[] aux = browsers.Split(new char[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string item in aux)
                {
                    list.Add(item);
                }
            }

            return list;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IList<string> HtmlSuites()
        {
            List<string> list = new List<string>();
            string testFP;
            // string htmlSuite = arguments["htmlSuite"];
            //if (!String.IsNullOrWhiteSpace(htmlSuite))
            //{
            //    list.Add(htmlSuite);
            //}

            testFP = arguments["htmlSuite"];
            SplitAndAddToList(ref list, testFP);

            testFP = arguments["htmlSuites"] ?? SeleneseGridRunnerConfiguration.HtmlSuites;
            SplitAndAddToList(ref list, testFP);

            testFP = arguments["t"];
            SplitAndAddToList(ref list, testFP);

            testFP = arguments["test"];
            SplitAndAddToList(ref list, testFP);

            return list;
        }

        private static void SplitAndAddToList(ref List<string> list, string testFP)
        {
            if (!String.IsNullOrWhiteSpace(testFP))
            {
                string[] aux = testFP.Split(new char[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var item in aux)
                {
                    list.Add(item);
                }
            }
        }
    }
}
