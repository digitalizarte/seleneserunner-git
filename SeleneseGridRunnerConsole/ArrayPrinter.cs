namespace SeleneseGridRunner
{
    using System;
    using System.IO;
    using System.Text;

    class ArrayPrinter
    {
        #region Declarations

        bool isLeftAligned = true;
        const string cellLeftTop = "┌";
        const string cellRightTop = "┐";
        const string cellLeftBottom = "└";
        const string cellRightBottom = "┘";
        const string cellHorizontalJointTop = "┬";
        const string cellHorizontalJointbottom = "┴";
        const string cellVerticalJointLeft = "├";
        const string cellTJoint = "┼";
        const string cellVerticalJointRight = "┤";
        const string cellHorizontalLine = "─";
        const string cellVerticalLine = "│";

        #endregion

        #region Private Methods

        private int[] GetMaxCellWidth(string[,] arrValues)
        {
            int[] maxWidth = new int[arrValues.GetLength(1)];

            for (int i = 0, il = arrValues.GetLength(0); i < il; i++)
            {
                for (int j = 0, jl = arrValues.GetLength(1); j < jl; j++)
                {
                    int length = arrValues[i, j].Trim().Length;
                    if (length > maxWidth[j])
                    {
                        maxWidth[j] = length;
                    }
                }
            }

            return maxWidth;
        }

        private string GetDataInTableFormat(string[,] arrValues)
        {
            string formattedString = string.Empty;

            if (arrValues == null)
                return formattedString;

            int dimension1Length = arrValues.GetLength(0);
            int dimension2Length = arrValues.GetLength(1);

            int[] maxCellWidth = GetMaxCellWidth(arrValues);
            // int indentLength = (dimension2Length * maxCellWidth) + (dimension2Length - 1);
            int indentLength = dimension2Length;

            for (int i = 0; i < maxCellWidth.Length; i++)
            {
                indentLength += maxCellWidth[i];
            }

            //printing top line;
            formattedString = string.Format("{0}{1}{2}{3}", cellLeftTop, Indent(indentLength), cellRightTop, Environment.NewLine);

            for (int i = 0; i < dimension1Length; i++)
            {
                string lineWithValues = cellVerticalLine;
                string line = cellVerticalJointLeft;
                for (int j = 0; j < dimension2Length; j++)
                {
                    string value = (isLeftAligned) ? arrValues[i, j].PadRight(maxCellWidth[j], ' ') : arrValues[i, j].PadLeft(maxCellWidth[j], ' ');
                    lineWithValues += String.Format("{0}{1}", value, cellVerticalLine);
                    line += Indent(maxCellWidth[j]);
                    if (j < (dimension2Length - 1))
                    {
                        line += cellTJoint;
                    }
                }
                line += cellVerticalJointRight;
                formattedString += String.Format("{0}{1}", lineWithValues, Environment.NewLine);
                if (i < (dimension1Length - 1))
                {
                    formattedString += String.Format("{0}{1}", line, Environment.NewLine);
                }
            }

            //printing bottom line
            formattedString += String.Format("{0}{1}{2}{3}", cellLeftBottom, Indent(indentLength), cellRightBottom, Environment.NewLine);
            return formattedString;
        }

        private string Indent(int count)
        {
            return string.Empty.PadLeft(count, '─');
        }

        #endregion

        #region Public Methods

        public void PrintToStream(string[,] arrValues, StreamWriter writer)
        {
            if (arrValues == null)
                return;

            if (writer == null)
                return;

            writer.Write(GetDataInTableFormat(arrValues));
        }

        public void PrintToConsole(string[,] arrValues)
        {
            if (arrValues == null)
                return;

            Console.WriteLine(GetDataInTableFormat(arrValues));
        }

        public void PrintToConsole(string[,] arrValues, ConsoleColor[,] colors)
        {
            if (arrValues == null)
            {
                return;
            }

            PrintDataInTableFormat(arrValues, colors);
        }

        private void PrintDataInTableFormat(string[,] arrValues, ConsoleColor[,] colors)
        {
            string formattedString = String.Empty;

            if (arrValues == null)
            {
                return;
            }

            int dimension1Length = arrValues.GetLength(0);
            int dimension2Length = arrValues.GetLength(1);

            int[] maxCellWidth = GetMaxCellWidth(arrValues);
            // int indentLength = (dimension2Length * maxCellWidth) + (dimension2Length - 1);
            int indentLength = 0;

            StringBuilder sbTop = new StringBuilder();
            StringBuilder sbBot = new StringBuilder();
            for (int i = 0, l = maxCellWidth.Length; i < l; i++)
            {
                indentLength += maxCellWidth[i];
                sbTop.Append(Indent(maxCellWidth[i]));
                sbBot.Append(Indent(maxCellWidth[i]));
                if (i < l - 1)
                {
                    sbTop.Append(cellHorizontalJointTop);
                    sbBot.Append(cellHorizontalJointbottom);
                }
            }

            //printing top line;
            // formattedString = string.Format("{0}{1}{2}{3}", cellLeftTop, Indent(indentLength), cellRightTop, Environment.NewLine);
            // Console.WriteLine("{0}{1}{2}", cellLeftTop, Indent(indentLength), cellRightTop);
            Console.WriteLine("{0}{1}{2}", cellLeftTop, sbTop, cellRightTop);

            for (int i = 0; i < dimension1Length; i++)
            {
                //string lineWithValues = cellVerticalLine;
                Console.Write(cellVerticalLine);
                string line = cellVerticalJointLeft;
                for (int j = 0; j < dimension2Length; j++)
                {
                    string value = (isLeftAligned) ? arrValues[i, j].PadRight(maxCellWidth[j], ' ') : arrValues[i, j].PadLeft(maxCellWidth[j], ' ');
                    // lineWithValues += string.Format("{0}{1}", value, cellVerticalLine);
                    ConsoleExtension.ColorWrite(colors[i, j], "{0}", value);
                    Console.Write(cellVerticalLine);
                    line += Indent(maxCellWidth[j]);
                    if (j < (dimension2Length - 1))
                    {
                        line += cellTJoint;
                    }
                }
                line += cellVerticalJointRight;
                // formattedString += string.Format("{0}{1}", lineWithValues, Environment.NewLine);
                Console.WriteLine();
                if (i < (dimension1Length - 1))
                {
                    // formattedString += string.Format("{0}{1}", line, Environment.NewLine);
                    Console.WriteLine(line);
                }
            }

            //printing bottom line
            // formattedString += string.Format("{0}{1}{2}{3}", cellLeftBottom, Indent(indentLength), cellRightBottom, Environment.NewLine);
            // Console.WriteLine("{0}{1}{2}", cellLeftBottom, Indent(indentLength), cellRightBottom);
            Console.WriteLine("{0}{1}{2}", cellLeftBottom, sbBot, cellRightBottom);
            // return formattedString;
        }

        #endregion
    }
}
