// addLocationStrategy
// [ERROR: Unsupported command [addLocationStrategy |  | ]]
// addScript
// [ERROR: Unsupported command [addScript |  | ]]
// addSelection
// [ERROR: Unsupported command [addSelection |  | ]]
// allowNativeXpath
// [ERROR: Unsupported command [allowNativeXpath |  | ]]
// altKeyDown
// [ERROR: Unsupported command [altKeyDown |  | ]]
// altKeyUp
// [ERROR: Unsupported command [altKeyUp |  | ]]
// answerOnNextPrompt
// [ERROR: Unsupported command [answerOnNextPrompt |  | ]]
// assignId
// [ERROR: Unsupported command [assignId |  | ]]
// break
// [ERROR: Unsupported command [break |  | ]]
// captureEntirePageScreenshot
// [ERROR: Unsupported command [captureEntirePageScreenshot |  | ]]
// check
if (!driver.FindElement(By.CssSelector("body")).Selected)
{
    driver.FindElement(By.CssSelector("body")).Click();
};
// chooseCancelOnNextConfirmation
acceptNextAlert = false;
// chooseOkOnNextConfirmation
acceptNextAlert = true;
// click
driver.FindElement(By.CssSelector("body")).Click();
// clickAt
// [ERROR: Unsupported command [clickAt |  | ]]
// close
driver.Close();
// contextMenu
// [ERROR: Unsupported command [contextMenu |  | ]]
// contextMenuAt
// [ERROR: Unsupported command [contextMenuAt |  | ]]
// controlKeyDown
// [ERROR: Unsupported command [controlKeyDown |  | ]]
// controlKeyUp
// [ERROR: Unsupported command [controlKeyUp |  | ]]
// createCookie
// [ERROR: Unsupported command [createCookie |  | ]]
// deleteAllVisibleCookies
// [ERROR: Unsupported command [deleteAllVisibleCookies |  | ]]
// deleteCookie
// [ERROR: Unsupported command [deleteCookie |  | ]]
// deselectPopUp
// [ERROR: Unsupported command [deselectPopUp |  | ]]
// doubleClick
// [ERROR: Unsupported command [doubleClick |  | ]]
// doubleClickAt
// [ERROR: Unsupported command [doubleClickAt |  | ]]
// dragAndDrop
// [ERROR: Unsupported command [dragAndDrop |  | ]]
// echo
Console.WriteLine("");
// fireEvent
// [ERROR: Unsupported command [fireEvent |  | ]]
// focus
// [ERROR: Unsupported command [focus |  | ]]
// goBack
driver.Navigate().Back();
// highlight
// [ERROR: Unsupported command [highlight |  | ]]
// ignoreAttributesWithoutValue
// [ERROR: Unsupported command [ignoreAttributesWithoutValue |  | ]]
// keyDown
// [ERROR: Unsupported command [keyDown |  | ]]
// keyPress
// [ERROR: Unsupported command [keyPress |  | ]]
// keyUp
// [ERROR: Unsupported command [keyUp |  | ]]
// metaKeyDown
// [ERROR: Unsupported command [metaKeyDown |  | ]]
// metaKeyUp
// [ERROR: Unsupported command [metaKeyUp |  | ]]
// mouseDown
// [ERROR: Unsupported command [mouseDown |  | ]]
// mouseDownAt
// [ERROR: Unsupported command [mouseDownAt |  | ]]
// mouseDownRight
// [ERROR: Unsupported command [mouseDownRight |  | ]]
// mouseDownRightAt
// [ERROR: Unsupported command [mouseDownRightAt |  | ]]
// mouseMove
// [ERROR: Unsupported command [mouseMove |  | ]]
// mouseMoveAt
// [ERROR: Unsupported command [mouseMoveAt |  | ]]
// mouseOut
// [ERROR: Unsupported command [mouseOut |  | ]]
// mouseOver
// [ERROR: Unsupported command [mouseOver |  | ]]
// mouseUp
// [ERROR: Unsupported command [mouseUp |  | ]]
// mouseUpAt
// [ERROR: Unsupported command [mouseUpAt |  | ]]
// mouseUpRight
// [ERROR: Unsupported command [mouseUpRight |  | ]]
// mouseUpRightAt
// [ERROR: Unsupported command [mouseUpRightAt |  | ]]
// open
driver.Navigate().GoToUrl(baseURL + "");
// openWindow
// [ERROR: Unsupported command [openWindow |  | ]]
// pause
// refresh
driver.Navigate().Refresh();
// removeAllSelections
// [ERROR: Unsupported command [removeAllSelections |  | ]]
// removeScript
// [ERROR: Unsupported command [removeScript |  | ]]
// removeSelection
// [ERROR: Unsupported command [removeSelection |  | ]]
// rollup
// [ERROR: Unsupported command [rollup |  | ]]
// runScript
// [ERROR: Unsupported command [runScript |  | ]]
// select
// [ReferenceError: selectLocator is not defined]
// selectFrame
// [ERROR: Unsupported command [selectFrame |  | ]]
// selectPopUp
// [ERROR: Unsupported command [selectPopUp |  | ]]
// selectWindow
// [ERROR: Unsupported command [selectWindow |  | ]]
// sendKeys
driver.FindElement(By.CssSelector("body")).SendKeys("");
// setBrowserLogLevel
// [ERROR: Unsupported command [setBrowserLogLevel |  | ]]
// setCursorPosition
// [ERROR: Unsupported command [setCursorPosition |  | ]]
// setMouseSpeed
// [ERROR: Unsupported command [setMouseSpeed |  | ]]
// setSpeed
// [ERROR: Unsupported command [setSpeed |  | ]]
// setTimeout
// [ERROR: Unsupported command [setTimeout |  | ]]
// shiftKeyDown
// [ERROR: Unsupported command [shiftKeyDown |  | ]]
// shiftKeyUp
// [ERROR: Unsupported command [shiftKeyUp |  | ]]
// submit
driver.FindElement(By.CssSelector("body")).Submit();
// type
driver.FindElement(By.CssSelector("body")).Clear();
driver.FindElement(By.CssSelector("body")).SendKeys("");
// typeKeys
// [ERROR: Unsupported command [typeKeys | css=body | ]]
// uncheck
if (driver.FindElement(By.CssSelector("body")).Selected)
{
    driver.FindElement(By.CssSelector("body")).Click();
};
// useXpathLibrary
// [ERROR: Unsupported command [useXpathLibrary |  | ]]
// waitForAlert


for (int second = 0;; second++) {
    if (second >= 60) Assert.Fail("timeout");
    try
    {
        if ("" == CloseAlertAndGetItsText()) break;
    }
    catch (Exception)
    {}
    Thread.Sleep(1000);
}
// waitForAlertNotPresent
for (int second = 0;; second++) {
    if (second >= 60) Assert.Fail("timeout");
    try
    {
        if (!IsAlertPresent()) break;
    }
    catch (Exception)
    {}
    Thread.Sleep(1000);
}
// waitForAlertPresent
for (int second = 0;; second++) {
    if (second >= 60) Assert.Fail("timeout");
    try
    {
        if (IsAlertPresent()) break;
    }
    catch (Exception)
    {}
    Thread.Sleep(1000);
}
// waitForAllButtons
// [ERROR: Unsupported command [getAllButtons |  | ]]
// waitForAllFields
// [ERROR: Unsupported command [getAllFields |  | ]]
// waitForAllLinks
// [ERROR: Unsupported command [getAllLinks |  | ]]
// waitForAllWindowIds
// [ERROR: Unsupported command [getAllWindowIds |  | ]]
// waitForAllWindowNames
// [ERROR: Unsupported command [getAllWindowNames |  | ]]
// waitForAllWindowTitles
// [ERROR: Unsupported command [getAllWindowTitles |  | ]]
// waitForAttribute
for (int second = 0;; second++) {
    if (second >= 60) Assert.Fail("timeout");
    try
    {
        if ("" == driver.FindElement(By.CssSelector("bod")).GetAttribute("css=body")) break;
    }
    catch (Exception)
    {}
    Thread.Sleep(1000);
}
// waitForAttributeFromAllWindows
// [ERROR: Unsupported command [getAttributeFromAllWindows |  | ]]
// waitForBodyText
for (int second = 0;; second++) {
    if (second >= 60) Assert.Fail("timeout");
    try
    {
        if ("" == driver.FindElement(By.TagName("BODY")).Text) break;
    }
    catch (Exception)
    {}
    Thread.Sleep(1000);
}
// waitForChecked
for (int second = 0;; second++) {
    if (second >= 60) Assert.Fail("timeout");
    try
    {
        if (driver.FindElement(By.CssSelector("body")).Selected) break;
    }
    catch (Exception)
    {}
    Thread.Sleep(1000);
}
// waitForConfirmation
for (int second = 0;; second++) {
    if (second >= 60) Assert.Fail("timeout");
    try
    {
        if ("" == CloseAlertAndGetItsText()) break;
    }
    catch (Exception)
    {}
    Thread.Sleep(1000);
}
// waitForConfirmationNotPresent
for (int second = 0;; second++) {
    if (second >= 60) Assert.Fail("timeout");
    try
    {
        if (!IsAlertPresent()) break;
    }
    catch (Exception)
    {}
    Thread.Sleep(1000);
}
// waitForConfirmationPresent
for (int second = 0;; second++) {
    if (second >= 60) Assert.Fail("timeout");
    try
    {
        if (IsAlertPresent()) break;
    }
    catch (Exception)
    {}
    Thread.Sleep(1000);
}
// waitForConfirmationNotPresent
for (int second = 0;; second++) {
    if (second >= 60) Assert.Fail("timeout");
    try
    {
        if (!IsAlertPresent()) break;
    }
    catch (Exception)
    {}
    Thread.Sleep(1000);
}
// waitForCookie
// [ERROR: Unsupported command [getCookie |  | ]]
// waitForCookieByName
// [ERROR: Unsupported command [getCookieByName |  | ]]
// waitForCookieNotPresent
// [ERROR: Unsupported command [isCookiePresent |  | ]]
// waitForCookiePresent
// [ERROR: Unsupported command [isCookiePresent |  | ]]
// waitForCssCount
for (int second = 0;; second++) {
    if (second >= 60) Assert.Fail("timeout");
    try
    {
        if ( == driver.FindElements(By.CssSelector("body")).Count) break;
    }
    catch (Exception)
    {}
    Thread.Sleep(1000);
}
// waitForCursorPosition
// [ERROR: Unsupported command [getCursorPosition |  | ]]
// waitForEditable
// [ERROR: Unsupported command [isEditable |  | ]]
// waitForElementHeight
// [ERROR: Unsupported command [getElementHeight |  | ]]
// waitForElementIndex
// [ERROR: Unsupported command [getElementIndex |  | ]]
// waitForElementNotPresent
for (int second = 0;; second++) {
    if (second >= 60) Assert.Fail("timeout");
    try
    {
        if (!IsElementPresent(By.CssSelector("body"))) break;
    }
    catch (Exception)
    {}
    Thread.Sleep(1000);
}
// waitForElementPositionLeft
// [ERROR: Unsupported command [getElementPositionLeft |  | ]]
// waitForElementPositionTop
// [ERROR: Unsupported command [getElementPositionTop |  | ]]
// waitForElementPresent
// [Error: locator strategy either id or name must be specified explicitly.]
// waitForElementWidth
// [ERROR: Unsupported command [getElementWidth |  | ]]
// waitForEval
// [ERROR: Unsupported command [getEval |  | ]]
// waitForExpression
// [ERROR: Unsupported command [getExpression |  | ]]
// waitForFrameToLoad
// [ERROR: Unsupported command [waitForFrameToLoad |  | ]]
// waitForHtmlSource
// [ERROR: Unsupported command [getHtmlSource |  | ]]
// waitForLocation
for (int second = 0;; second++) {
    if (second >= 60) Assert.Fail("timeout");
    try
    {
        if ("" == driver.Url) break;
    }
    catch (Exception)
    {}
    Thread.Sleep(1000);
}
// waitForMouseSpeed
// [ERROR: Unsupported command [getMouseSpeed |  | ]]
// waitForNotAlert
for (int second = 0;; second++) {
    if (second >= 60) Assert.Fail("timeout");
    try
    {
        if ("" != CloseAlertAndGetItsText()) break;
    }
    catch (Exception)
    {}
    Thread.Sleep(1000);
}
// waitForNotAllButtons
// [ERROR: Unsupported command [getAllButtons |  | ]]
// waitForNotAllFields
// [ERROR: Unsupported command [getAllFields |  | ]]
// waitForNotAllLinks
// [ERROR: Unsupported command [getAllLinks |  | ]]
// waitForNotAllWindowIds
// [ERROR: Unsupported command [getAllWindowIds |  | ]]
// waitForNotAllWindowNames
// [ERROR: Unsupported command [getAllWindowNames |  | ]]
// waitForNotAllWindowTitles
// [ERROR: Unsupported command [getAllWindowTitles |  | ]]
// waitForNotAttribute
for (int second = 0;; second++) {
    if (second >= 60) Assert.Fail("timeout");
    try
    {
        if ("" != driver.FindElement(By.CssSelector("bod")).GetAttribute("css=body")) break;
    }
    catch (Exception)
    {}
    Thread.Sleep(1000);
}
// waitForNotAttributeFromAllWindows
// [ERROR: Unsupported command [getAttributeFromAllWindows |  | ]]
// waitForNotBodyText
for (int second = 0;; second++) {
    if (second >= 60) Assert.Fail("timeout");
    try
    {
        if ("" != driver.FindElement(By.TagName("BODY")).Text) break;
    }
    catch (Exception)
    {}
    Thread.Sleep(1000);
}
// waitForNotChecked
for (int second = 0;; second++) {
    if (second >= 60) Assert.Fail("timeout");
    try
    {
        if (!driver.FindElement(By.CssSelector("body")).Selected) break;
    }
    catch (Exception)
    {}
    Thread.Sleep(1000);
}
// waitForNotConfirmation
for (int second = 0;; second++) {
    if (second >= 60) Assert.Fail("timeout");
    try
    {
        if ("" != CloseAlertAndGetItsText()) break;
    }
    catch (Exception)
    {}
    Thread.Sleep(1000);
}
// waitForNotCookie
// [ERROR: Unsupported command [getCookie |  | ]]
// waitForNotCookieByName
// [ERROR: Unsupported command [getCookieByName |  | ]]
// waitForNotCssCount
for (int second = 0;; second++) {
    if (second >= 60) Assert.Fail("timeout");
    try
    {
        if ( != driver.FindElements(By.CssSelector("body")).Count) break;
    }
    catch (Exception)
    {}
    Thread.Sleep(1000);
}
// waitForNotCursorPosition
// [ERROR: Unsupported command [getCursorPosition |  | ]]
// waitForNotEditable
// [ERROR: Unsupported command [isEditable |  | ]]
// waitForNotElementHeight
// [ERROR: Unsupported command [getElementHeight |  | ]]
// waitForNotElementIndex
// [ERROR: Unsupported command [getElementIndex |  | ]]
// waitForNotElementPositionLeft
// [ERROR: Unsupported command [getElementPositionLeft |  | ]]
// waitForNotElementPositionTop
// [ERROR: Unsupported command [getElementPositionTop |  | ]]
// waitForNotElementWidth
// [ERROR: Unsupported command [getElementWidth |  | ]]
// waitForNotEval
// [ERROR: Unsupported command [getEval |  | ]]
// waitForNotExpression
// [ERROR: Unsupported command [getExpression |  | ]]
// waitForNotHtmlSource
// [ERROR: Unsupported command [getHtmlSource |  | ]]
// waitForNotLocation
for (int second = 0;; second++) {
    if (second >= 60) Assert.Fail("timeout");
    try
    {
        if ("" != driver.Url) break;
    }
    catch (Exception)
    {}
    Thread.Sleep(1000);
}
// waitForNotMouseSpeed
// [ERROR: Unsupported command [getMouseSpeed |  | ]]
// waitForNotPrompt
// [ERROR: Unsupported command [getPrompt |  | ]]
// waitForNotSelectOptions
// [ERROR: Unsupported command [getSelectOptions |  | ]]
// waitForNotSelectedId
// [ERROR: Unsupported command [getSelectedId |  | ]]
// waitForNotSelectedIds
// [ERROR: Unsupported command [getSelectedIds |  | ]]
// waitForNotSelectedIndex
// [ERROR: Unsupported command [getSelectedIndex |  | ]]
// waitForNotSelectedIndex
// [ERROR: Unsupported command [getSelectedIndex |  | ]]
// waitForNotSelectedIndexes
// [ERROR: Unsupported command [getSelectedIndexes |  | ]]
// waitForNotSelectedLabel
// [ERROR: Unsupported command [getSelectedLabel |  | ]]
// waitForNotSelectedLabels
// [ERROR: Unsupported command [getSelectedLabels |  | ]]
// waitForNotSelectedValue
// [ERROR: Unsupported command [getSelectedValue |  | ]]
// waitForNotSelectedValues
// [ERROR: Unsupported command [getSelectedValues |  | ]]
// waitForNotSomethingSelected
// [ERROR: Unsupported command [isSomethingSelected |  | ]]
// waitForNotSpeed
// [ERROR: Unsupported command [getSpeed |  | ]]
// waitForNotTable
// [ERROR: Unsupported command [getTable |  | ]]
// waitForNotText
for (int second = 0;; second++) {
    if (second >= 60) Assert.Fail("timeout");
    try
    {
        if ("" != driver.FindElement(By.CssSelector("body")).Text) break;
    }
    catch (Exception)
    {}
    Thread.Sleep(1000);
}
// waitForNotTitle
for (int second = 0;; second++) {
    if (second >= 60) Assert.Fail("timeout");
    try
    {
        if ("" != driver.Title) break;
    }
    catch (Exception)
    {}
    Thread.Sleep(1000);
}
// waitForNotValue
for (int second = 0;; second++) {
    if (second >= 60) Assert.Fail("timeout");
    try
    {
        if ("value" != driver.FindElement(By.CssSelector("body")).GetAttribute("value")) break;
    }
    catch (Exception)
    {}
    Thread.Sleep(1000);
}
// waitForNotVisible
for (int second = 0;; second++) {
    if (second >= 60) Assert.Fail("timeout");
    try
    {
        if (!driver.FindElement(By.CssSelector("body")).Displayed) break;
    }
    catch (Exception)
    {}
    Thread.Sleep(1000);
}
// waitForNotWhetherThisFrameMatchFrameExpression
// [TypeError: pattern is null]
// waitForNotWhetherThisWindowMatchWindowExpression
// [TypeError: pattern is null]
// waitForNotXpathCount
for (int second = 0;; second++) {
    if (second >= 60) Assert.Fail("timeout");
    try
    {
        if ( != driver.FindElements(By.CssSelector("body")).Count) break;
    }
    catch (Exception)
    {}
    Thread.Sleep(1000);
}
// waitForOrdered
// [ERROR: Unsupported command [isOrdered |  | ]]
// waitForPageToLoad
// waitForPopUp
// [ERROR: Unsupported command [waitForPopUp |  | ]]
// waitForPrompt
// [ERROR: Unsupported command [getPrompt |  | ]]
// waitForPromptNotPresent
// [ERROR: Unsupported command [isPromptPresent |  | ]]
// waitForPromptPresent
// [ERROR: Unsupported command [isPromptPresent |  | ]]
// waitForSelectOptions
// [ERROR: Unsupported command [getSelectOptions |  | ]]
// waitForSelectedId
// [ERROR: Unsupported command [getSelectedId |  | ]]
// waitForSelectedIds
// [ERROR: Unsupported command [getSelectedIds |  | ]]
// waitForSelectedIndex
// [ERROR: Unsupported command [getSelectedIndex |  | ]]
// waitForSelectedIndexes
// [ERROR: Unsupported command [getSelectedIndexes |  | ]]
// waitForSelectedLabel
// [ERROR: Unsupported command [getSelectedLabel |  | ]]
// waitForSelectedLabels
// [ERROR: Unsupported command [getSelectedLabels |  | ]]
// waitForSelectedValue
// [ERROR: Unsupported command [getSelectedValue |  | ]]
// waitForSelectedValues
// [ERROR: Unsupported command [getSelectedValues |  | ]]
// waitForTable
// [ERROR: Unsupported command [getTable |  | ]]
// waitForText
for (int second = 0;; second++) {
    if (second >= 60) Assert.Fail("timeout");
    try
    {
        if (Regex.IsMatch(driver.FindElement(By.CssSelector("body")).Text, "^algo[\\s\\S]*$")) break;
    }
    catch (Exception)
    {}
    Thread.Sleep(1000);
}
// waitForTextNotPresent
// Warning: waitForTextNotPresent may require manual changes
for (int second = 0;; second++) {
    if (second >= 60) Assert.Fail("timeout");
    try
    {
        if (!Regex.IsMatch(driver.FindElement(By.CssSelector("BODY")).Text, "^[\\s\\S]*css=body[\\s\\S]*$")) break;
    }
    catch (Exception)
    {}
    Thread.Sleep(1000);
}
// waitForTextPresent
// Warning: waitForTextPresent may require manual changes
for (int second = 0;; second++) {
    if (second >= 60) Assert.Fail("timeout");
    try
    {
        if (Regex.IsMatch(driver.FindElement(By.CssSelector("BODY")).Text, "^[\\s\\S]*css=body[\\s\\S]*$")) break;
    }
    catch (Exception)
    {}
    Thread.Sleep(1000);
}
// waitForTitle
for (int second = 0;; second++) {
    if (second >= 60) Assert.Fail("timeout");
    try
    {
        if ("" == driver.Title) break;
    }
    catch (Exception)
    {}
    Thread.Sleep(1000);
}
// waitForValue
for (int second = 0;; second++) {
    if (second >= 60) Assert.Fail("timeout");
    try
    {
        if ("" == driver.FindElement(By.CssSelector("body")).GetAttribute("value")) break;
    }
    catch (Exception)
    {}
    Thread.Sleep(1000);
}
// waitForVisible
for (int second = 0;; second++) {
    if (second >= 60) Assert.Fail("timeout");
    try
    {
        if (driver.FindElement(By.CssSelector("body")).Displayed) break;
    }
    catch (Exception)
    {}
    Thread.Sleep(1000);
}
// waitForWhetherThisFrameMatchFrameExpression
// [TypeError: pattern is null]
// waitForWhetherThisWindowMatchWindowExpression
// [TypeError: pattern is null]
// waitForXpathCount
for (int second = 0;; second++) {
    if (second >= 60) Assert.Fail("timeout");
    try
    {
        if (0 == driver.FindElements(By.CssSelector("body")).Count) break;
    }
    catch (Exception)
    {}
    Thread.Sleep(1000);
}
// windowFocus
// [ERROR: Unsupported command [windowFocus |  | ]]
// windowMaximize
// [ERROR: Unsupported command [windowMaximize |  | ]]
