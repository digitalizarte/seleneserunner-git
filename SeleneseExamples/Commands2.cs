private static void ChooseCancelOnNextConfirmation(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
{
    if (cmd == null)
    {
	throw new ArgumentNullException("cmd");
    }

    if (drv == null)
    {
	throw new ArgumentNullException("drv");
    }

    if (url == null)
    {
	throw new ArgumentNullException("url");
    }

    if (ste == null)
    {
	throw new ArgumentNullException("ste");
    }

    acceptNextAlert = false;

}

private static void WaitForAlert(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
{
    if (cmd == null)
    {
	throw new ArgumentNullException("cmd");
    }

    if (drv == null)
    {
	throw new ArgumentNullException("drv");
    }

    if (url == null)
    {
	throw new ArgumentNullException("url");
    }

    if (ste == null)
    {
	throw new ArgumentNullException("ste");
    }

	string arg1 = StateHelper.Parse(cmd.Argument1, ste),
                   arg2 = StateHelper.Parse(cmd.Argument2, ste);
            By locator = LocatorHelper.Parse(arg1);
            IWebElement element = drv.FindElementDeep(locator);
    
	if (!element .Selected)
	{    

            if (HighlightElement)
            {
                element.HighlightElement();
            }
            element.Focus();
            element.Click();
	}
	
	drv.WaitForCondition(()=> "" == CloseAlertAndGetItsText(AcceptNextAlert));

}


ChooseCancelOnNextConfirmation
ChooseOkOnNextConfirmation
