namespace SeleneseGridRunner
{
    using System;
    using System.Text;

    /// <summary>
    /// Representa un comando selenese.
    /// </summary>
    public class TestCommand : ITestCommand
    {
        private readonly string command;
        private readonly string argument1;
        private readonly string argument2;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        /// <param name="argument1"></param>
        /// <param name="argument2"></param>
        public TestCommand(string command, string argument1, string argument2)
        {
            this.command = command;
            this.argument1 = argument1;
            this.argument2 = argument2;
        }

        /// <summary>
        /// Nombre del comando.
        /// </summary>
        public virtual string Command
        {
            get
            {
                return this.command;
            }
        }

        /// <summary>
        /// Argumento 1 que recibe el comando.
        /// </summary>
        public virtual string Argument1
        {
            get
            {
                return this.argument1;
            }
        }

        /// <summary>
        /// Argumento 2 que recibe el comando.
        /// </summary>
        public virtual string Argument2
        {
            get
            {
                return this.argument2;
            }
        }

        public override string ToString()
        {
            // return String.Format("{{ Command: \"{0}\", argument1: \"{1}\", argument2: \"{2}\" }}", this.command, this.argument1, this.argument2);
            StringBuilder sb = new StringBuilder();
            sb.Append(this.command);
            if (!String.IsNullOrWhiteSpace(this.argument1))
            {
                if (!String.IsNullOrWhiteSpace(this.argument2))
                {
                    sb.AppendFormat("({0}, {1})", this.argument1, this.argument2);
                }
                else
                {
                    sb.AppendFormat("({0})", this.argument1);
                }
            }
            else if (!String.IsNullOrWhiteSpace(this.argument2))
            {
                sb.AppendFormat("({0})", this.argument2);
            }
            else
            {
                sb.Append("()");
            }
            return sb.ToString();
        }
    }
}
