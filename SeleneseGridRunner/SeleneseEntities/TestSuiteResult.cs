﻿namespace SeleneseGridRunner
{
    using System;
    using System.Collections.Generic;
    using NUnit.Framework;
    using System.Diagnostics;

    public class TestSuiteResult : ITestSuiteBase
    {
        private readonly TestSuite testSuite;
        private readonly string baseUrl;
        private readonly IList<TestCaseResult> testCaseResults;
        private readonly string browser;
        private readonly NUnit.Framework.TestStatus status;
        private readonly Exception exception;
        private readonly TimeSpan elapsed;


        /// <summary>
        /// Inicia el resultado de la ejecución una suite de pruebas.
        /// </summary>
        /// <param name="testSuite">Suite de pruebas.</param>
        /// <param name="testCaseResults">Lista de resultado de la ejecución de los casos de pruebas.</param>
        /// <param name="baseUrl">Url base de las pruebas.</param>
        /// <param name="browser">Navegador en que se realizaron las pruebas.</param>
        /// <param name="status">Estado final de la ejecúción de la suite de pruebas.</param>
        /// <param name="elapsed">Tiempo que demora la ejecución de la suite.</param>
        /// <param name="ex">Excepcion producida al ejecutar la suite de pruebas.</param>
        public TestSuiteResult(TestSuite testSuite,
                                IList<TestCaseResult> testCaseResults,
                                string baseUrl,
                                string browser,
                                TestStatus status,
                                TimeSpan elapsed,
                                Exception ex = null)
        {
            this.testSuite = testSuite;
            this.baseUrl = baseUrl;
            this.status = status;
            this.browser = browser;
            this.testCaseResults = testCaseResults;
            this.elapsed = elapsed;
            this.exception = ex;
        }

        public virtual string FilePath
        {
            get
            {
                return this.testSuite.FilePath;
            }
        }

        public virtual string BaseUrl
        {
            get
            {
                return this.baseUrl;
            }
        }

        public virtual string Browser
        {
            get
            {
                return browser;
            }
        }

        public virtual TestStatus Status
        {
            get
            {
                return status;
            }
        }

        public virtual IList<TestCaseResult> TestCaseResults
        {
            get
            {
                return this.testCaseResults;
            }
        }

        public virtual Exception Exception
        {
            get
            {
                return exception;
            }
        }

        public TimeSpan Elapsed
        {
            get
            {
                return this.elapsed;
            }
        }
    }
}
