﻿namespace SeleneseGridRunner
{
    using System;
    using NUnit.Framework;
    using System.Diagnostics;

    public class TestCommandResult : ITestCommand
    {
        private TestStatus status;
        private Exception exception;
        private readonly TestCommand testCommand;
        private readonly string baseUrl;
        private readonly TimeSpan elapsed;
        public TestCommandResult(TestCommand testCommand, string baseUrl, TimeSpan elapsed)
        {
            this.status = TestStatus.Passed;
            this.testCommand = testCommand;
            this.baseUrl = baseUrl;
            this.elapsed = elapsed;
        }

        /// <summary>
        /// Nombre del comando.
        /// </summary>
        public string Command
        {
            get
            {
                return this.testCommand.Command;
            }

        }

        /// <summary>
        /// Argumento 1 que recibe el comando.
        /// </summary>
        public string Argument1
        {
            get
            {
                return this.testCommand.Argument1;
            }
        }

        /// <summary>
        /// Argumento 2 que recibe el comando.
        /// </summary>
        public string Argument2
        {
            get
            {
                return this.testCommand.Argument2;
            }
        }

        public TestStatus Status
        {
            get
            {
                return this.status;
            }
            set
            {
                this.status = value;
            }
        }

        public Exception Exception
        {
            get
            {
                return this.exception;
            }
            set
            {
                this.exception = value;
            }
        }

        public TimeSpan Elapsed
        {
            get
            {
                return this.elapsed;
            }
        }
    }
}