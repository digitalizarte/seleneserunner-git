﻿namespace SeleneseGridRunner
{
    using System.Collections.Generic;

    /// <summary>
    /// 
    /// </summary>
    public class TestSuite : ITestSuite
    {
        private readonly string filePath;
        private readonly IList<TestCase> testCases;

        /// <summary>
        /// Inicializa una suite de pruebas.
        /// </summary>
        /// <param name="testCases">Lista de casos de prueba.</param>
        /// <param name="filePath">Ruta del archivo HTML.</param>
        public TestSuite(IList<TestCase> testCases, string filePath)
        {
            
            this.testCases = testCases;
            this.filePath = filePath;
        }

        /// <summary>
        /// Ruta del archivo HTML.
        /// </summary>
        public virtual string FilePath
        {
            get
            {
                return filePath;
            }
        }
        
        /// <summary>
        /// Lista de casos de prueba.
        /// </summary>
        public virtual IList<TestCase> TestCases
        {
            get
            {
                return testCases;
            }
        }
    }
}