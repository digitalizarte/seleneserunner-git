﻿namespace SeleneseGridRunner
{
    using System.Collections.Generic;

    public class TestCase : ITestCase
    {
        private readonly IList<TestCommand> testCommands;
        private readonly string baseUrl;
        private readonly string name;
        private readonly string filePath;

        public TestCase(string name, string filePath, string baseUrl, IList<TestCommand> testCommands)
        {
            this.name = name;
            this.filePath = filePath;
            this.testCommands = testCommands;
            this.baseUrl = baseUrl;
        }

        public IList<TestCommand> TestCommands
        {
            get
            {
                return testCommands;
            }
        }

        public string BaseUrl
        {
            get
            {
                return this.baseUrl;
            }
        }

        public string Name
        {
            get
            {
                return this.name;
            }
        }

        public string FilePath
        {
            get
            {
                return this.filePath;
            }
        }
    }
}