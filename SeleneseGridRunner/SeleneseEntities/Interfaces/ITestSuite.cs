namespace SeleneseGridRunner
{
    using System.Collections.Generic;

    public interface ITestSuite : ITestSuiteBase
    {        
        IList<TestCase> TestCases { get; }
    }
}
