namespace SeleneseGridRunner
{
    using System.Collections.Generic;

    public interface ITestSuiteBase
    {
        string FilePath { get; }
    }
}
