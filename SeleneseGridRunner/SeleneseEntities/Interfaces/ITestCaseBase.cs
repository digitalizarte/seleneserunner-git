namespace SeleneseGridRunner
{
    using System.Collections.Generic;

    public interface ITestCaseBase
    {

        string BaseUrl { get; }
        string Name { get; }
        string FilePath { get; }
    }
}
