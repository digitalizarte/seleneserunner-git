namespace SeleneseGridRunner
{
    using System.Collections.Generic;

    public interface ITestCase : ITestCaseBase
    {
        IList<TestCommand> TestCommands { get; }
    }
}
