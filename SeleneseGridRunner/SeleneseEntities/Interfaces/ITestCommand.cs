namespace SeleneseGridRunner
{
    using System;

    public interface ITestCommand
    {
        string Command { get; }
        string Argument1 { get; }
        string Argument2 { get; }
    }
}
