﻿namespace SeleneseGridRunner
{
    using System.Collections.Generic;
    using NUnit.Framework;
    using System.Diagnostics;
    using System;

    /// <summary>
    /// Resultado de la ejecución de un caso de prueba.
    /// </summary>
    public class TestCaseResult : ITestCaseBase
    {
        private readonly TestCase testCase;
        private readonly IList<TestCommandResult> testCommandResults;
        private readonly TestStatus status;
        private readonly TimeSpan elapsed;

        /// <summary>
        /// Inicia el resultado de la ejecución de un caso de prueba.
        /// </summary>
        /// <param name="testCase">Caso de prueba.</param>
        /// <param name="testCommandResult">Lista de resultado de la ejecución de los comandos.</param>
        /// <param name="status">Estado final de la ejecúción del caso de prueba.</param>
        /// <param name="elapsed">Tiempo que demora la ejecución de el TestCase.</param>
        public TestCaseResult(TestCase testCase, IList<TestCommandResult> testCommandResult, TestStatus status, TimeSpan elapsed)
        {
            this.testCase = testCase;
            this.testCommandResults = testCommandResult;
            this.status = status;
            this.elapsed = elapsed;
        }

        public virtual string Name
        {
            get
            {
                return this.testCase.Name;
            }
        }

        public virtual string FilePath
        {
            get
            {
                return this.testCase.FilePath;
            }
        }

        public virtual IList<TestCommandResult> TestCommandResults
        {
            get
            {
                return testCommandResults;
            }
        }

        public virtual TestStatus Status
        {
            get
            {
                return status;
            }
        }

        public virtual string BaseUrl
        {
            get
            {
                return this.testCase.BaseUrl;
            }
        }
        
        public TimeSpan Elapsed
        {
            get
            {
                return this.elapsed;
            }
        }
    }
}