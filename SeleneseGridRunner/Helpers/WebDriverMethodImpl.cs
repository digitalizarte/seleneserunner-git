namespace SeleneseGridRunner
{
    using System;
    using System.Collections.Generic;
    using OpenQA.Selenium;

    class WebDriverMethodImpl : Tuple<string, Action<TestCommand, IWebDriver, string, IDictionary<string, object>>>
    {
        public WebDriverMethodImpl(string item1, 
                                   Action<TestCommand, IWebDriver, string, IDictionary<string, object>> item2)
            : base(item1, item2)
        {

        }
    }
}
