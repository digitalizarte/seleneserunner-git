namespace SeleneseGridRunner
{
    using EeekSoft.Text;
    using NUnit.Framework;
    using OpenQA.Selenium;
    using OpenQA.Selenium.Support.UI;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Globalization;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Threading;

    public static class CommandHelper
    {
        private static readonly object syncLock = new object();
        private static readonly Dictionary<string, Action<TestCommand, IWebDriver, string, IDictionary<string, object>>> actions = new Dictionary<string, Action<TestCommand, IWebDriver, string, IDictionary<string, object>>>(StringComparer.InvariantCultureIgnoreCase);
        private static readonly Dictionary<string, TestCommand> commands = new Dictionary<string, TestCommand>();

        [ThreadStatic]
        private static bool? highlightElement;

        public static bool HighlightElement
        {
            get
            {
                if (!highlightElement.HasValue)
                {
                    highlightElement = false;
                }
                return highlightElement.Value;
            }
            set
            {
                highlightElement = value;
            }
        }

        [ThreadStatic]
        private static bool? acceptNextAlert;

        public static bool AcceptNextAlert
        {
            get
            {
                if (!acceptNextAlert.HasValue)
                {
                    acceptNextAlert = true;
                }
                return acceptNextAlert.Value;
            }
            set
            {
                acceptNextAlert = value;
            }
        }

        static CommandHelper()
        {
            actions.Add("assertAlert", CommandHelper.AssertVerifyAlertConfirmation);
            actions.Add("verifyAlert", CommandHelper.AssertVerifyAlertConfirmation);
            actions.Add("assertAlertNotPresent", CommandHelper.AssertVerifyAlertConfirmationNotPresent);
            actions.Add("verifyAlertNotPresent", CommandHelper.AssertVerifyAlertConfirmationNotPresent);
            actions.Add("assertAlertPresent", CommandHelper.AssertVerifyAlertConfirmationPresent);
            actions.Add("verifyAlertPresent", CommandHelper.AssertVerifyAlertConfirmationPresent);
            actions.Add("assertAttribute", CommandHelper.AssertVerifyAttribute);
            actions.Add("verifyAttribute", CommandHelper.AssertVerifyAttribute);
            actions.Add("assertBodyText", CommandHelper.AssertVerifyBodyText);
            actions.Add("verifyBodyText", CommandHelper.AssertVerifyBodyText);
            actions.Add("assertChecked", CommandHelper.AssertVerifyChecked);
            actions.Add("verifyChecked", CommandHelper.AssertVerifyChecked);
            actions.Add("assertConfirmation", CommandHelper.AssertVerifyAlertConfirmation);
            actions.Add("verifyConfirmation", CommandHelper.AssertVerifyAlertConfirmation);
            actions.Add("assertConfirmationNotPresent", CommandHelper.AssertVerifyAlertConfirmationNotPresent);
            actions.Add("verifyConfirmationNotPresent", CommandHelper.AssertVerifyAlertConfirmationNotPresent);
            actions.Add("assertConfirmationPresent", CommandHelper.AssertVerifyAlertConfirmationPresent);
            actions.Add("verifyConfirmationPresent", CommandHelper.AssertVerifyAlertConfirmationPresent);
            actions.Add("assertCssCount", CommandHelper.AssertVerifyCssCount);
            actions.Add("verifyCssCount", CommandHelper.AssertVerifyCssCount);
            actions.Add("assertElementNotPresent", CommandHelper.AssertVerifyElementNotPresent);
            actions.Add("verifyElementNotPresent", CommandHelper.AssertVerifyElementNotPresent);
            actions.Add("assertElementPresent", CommandHelper.AssertVerifyElementPresent);
            actions.Add("verifyElementPresent", CommandHelper.AssertVerifyElementPresent);
            actions.Add("assertLocation", CommandHelper.AssertVerifyLocation);
            actions.Add("verifyLocation", CommandHelper.AssertVerifyLocation);
            actions.Add("assertNotLocation", CommandHelper.AssertVerifyNotLocation);
            actions.Add("verifyNotLocation", CommandHelper.AssertVerifyNotLocation);
            actions.Add("assertNotAlert", CommandHelper.AssertVerifyNotAlertConfirmation);
            actions.Add("verifyNotAlert", CommandHelper.AssertVerifyNotAlertConfirmation);
            actions.Add("assertNotAttribute", CommandHelper.AssertVerifyNotAttribute);
            actions.Add("verifyNotAttribute", CommandHelper.AssertVerifyNotAttribute);
            actions.Add("assertNotBodyText", CommandHelper.AssertVerifyNotBodyText);
            actions.Add("verifyNotBodyText", CommandHelper.AssertVerifyNotBodyText);
            actions.Add("assertNotChecked", CommandHelper.AssertVerifyNotChecked);
            actions.Add("verifyNotChecked", CommandHelper.AssertVerifyNotChecked);
            actions.Add("assertNotConfirmation", CommandHelper.AssertVerifyNotAlertConfirmation);
            actions.Add("verifyNotConfirmation", CommandHelper.AssertVerifyNotAlertConfirmation);
            actions.Add("assertNotCssCount", CommandHelper.AssertVerifyNotCssCount);
            actions.Add("verifyNotCssCount", CommandHelper.AssertVerifyNotCssCount);
            actions.Add("assertNotVisible", CommandHelper.AssertVerifyNotVisible);
            actions.Add("verifyNotVisible", CommandHelper.AssertVerifyNotVisible);
            actions.Add("assertText", CommandHelper.AssertVerifyText);
            actions.Add("verifyText", CommandHelper.AssertVerifyText);
            actions.Add("assertTextNotPresent", CommandHelper.AssertVerifyTextNotPresent);
            actions.Add("verifyTextNotPresent", CommandHelper.AssertVerifyTextNotPresent);
            actions.Add("assertTextPresent", CommandHelper.AssertVerifyTextPresent);
            actions.Add("verifyTextPresent", CommandHelper.AssertVerifyTextPresent);
            actions.Add("assertValue", CommandHelper.AssertVerifyValue);
            actions.Add("verifyValue", CommandHelper.AssertVerifyValue);
            actions.Add("assertNotValue", CommandHelper.AssertVerifyNotValue);
            actions.Add("verifyNotValue", CommandHelper.AssertVerifyNotValue);
            actions.Add("assertTitle", CommandHelper.AssertVerifyTitle);
            actions.Add("verifyTitle", CommandHelper.AssertVerifyTitle);
            actions.Add("assertNotTitle", CommandHelper.AssertVerifyNotTitle);
            actions.Add("verifyNotTitle", CommandHelper.AssertVerifyNotTitle);
            actions.Add("assertVisible", CommandHelper.AssertVerifyVisible);
            actions.Add("verifyVisible", CommandHelper.AssertVerifyVisible);
            actions.Add("assertXpathCount", CommandHelper.AssertVerifyXpathCount);
            actions.Add("verifyXpathCount", CommandHelper.AssertVerifyXpathCount);
            actions.Add("assertNotXpathCount", CommandHelper.AssertVerifyNotXpathCount);
            actions.Add("verifyNotXpathCount", CommandHelper.AssertVerifyNotXpathCount);
            actions.Add("assertNotText", CommandHelper.AssertVerifyNotText);
            actions.Add("verifyNotText", CommandHelper.AssertVerifyNotText);

            actions.Add("clear", CommandHelper.Clear);
            actions.Add("clearAndWait", CommandHelper.Clear);
            actions.Add("click", CommandHelper.Click);
            actions.Add("clickAndWait", CommandHelper.Click);
            actions.Add("echo", CommandHelper.Echo);

            actions.Add("chooseCancelOnNextConfirmation", CommandHelper.ChooseCancelOnNextConfirmation);
            actions.Add("chooseOkOnNextConfirmation", CommandHelper.ChooseOkOnNextConfirmation);
            actions.Add("goBack", CommandHelper.GoBack);
            actions.Add("goBackAndWait", CommandHelper.GoBack);

            actions.Add("refresh", CommandHelper.Refresh);
            actions.Add("refreshAndWait", CommandHelper.Refresh);

            actions.Add("check", CommandHelper.Check);
            actions.Add("checkAndWait", CommandHelper.Check);

            actions.Add("uncheck", CommandHelper.UnCheck);
            actions.Add("uncheckAndWait", CommandHelper.UnCheck);

            actions.Add("focus", CommandHelper.Focus);
            actions.Add("focusAndWait", CommandHelper.Focus);
            actions.Add("open", CommandHelper.Open);
            actions.Add("openAndWait", CommandHelper.Open);
            actions.Add("pause", CommandHelper.Pause);
            actions.Add("sendKeys", CommandHelper.SendKeys);
            actions.Add("sendKeysAndWait", CommandHelper.SendKeys);
            actions.Add("submit", CommandHelper.Submit);
            actions.Add("submitAndWait", CommandHelper.Submit);
            actions.Add("type", CommandHelper.Type);
            actions.Add("typeAndWait", CommandHelper.Type);
            actions.Add("setSpeed", CommandHelper.Dummy);

            actions.Add("store", CommandHelper.Store);
            actions.Add("storeNextWeek", CommandHelper.StoreNextWeek);
            actions.Add("storeToday", CommandHelper.StoreToday);
            actions.Add("storeTomorrow", CommandHelper.StoreTomorrow);
            actions.Add("storeText", CommandHelper.StoreText);
            actions.Add("storeAlert", CommandHelper.StoreAlertConfirmation);
            actions.Add("storeConfirmation", CommandHelper.StoreAlertConfirmation);
            actions.Add("storeAlertPresent", CommandHelper.StoreAlertConfirmationPresent);
            actions.Add("storeConfirmationPresent", CommandHelper.StoreAlertConfirmationPresent);
            actions.Add("storeAttribute", CommandHelper.StoreAttribute);
            actions.Add("storeBodyText", CommandHelper.StoreBodyText);
            actions.Add("storeChecked", CommandHelper.StoreChecked);
            actions.Add("storeCssCount", CommandHelper.StoreCssCount);
            actions.Add("storeElementPresent", CommandHelper.StoreElementPresent);
            actions.Add("storeLocation", CommandHelper.StoreLocation);
            actions.Add("storeTitle", CommandHelper.StoreTitle);
            actions.Add("storeValue", CommandHelper.StoreValue);
            actions.Add("storeVisible", CommandHelper.StoreVisible);
            actions.Add("storeXpathCount", CommandHelper.StoreXpathCount);

            actions.Add("waitForElementPresent", CommandHelper.WaitForElementPresent);
            actions.Add("waitForElementNotPresent", CommandHelper.WaitForElementNotPresent);
            actions.Add("waitForPageToLoad", CommandHelper.WaitForPageToLoad);
            // actions.Add("waitForVisible", CommandHelper.WaitForVisible);
            actions.Add("waitForAlert", CommandHelper.WaitForAlertConfirmation);
            actions.Add("waitForConfirmation", CommandHelper.WaitForAlertConfirmation);
            actions.Add("waitForAlertNotPresent", CommandHelper.WaitForAlertConfirmationNotPresent);
            actions.Add("waitForConfirmationNotPresent", CommandHelper.WaitForAlertConfirmationNotPresent);
            actions.Add("waitForAlertPresent", CommandHelper.WaitForAlertConfirmationPresent);
            actions.Add("waitForConfirmationPresent", CommandHelper.WaitForAlertConfirmationPresent);
            actions.Add("waitForAttribute", CommandHelper.WaitForAttribute);
            actions.Add("waitForBodyText", CommandHelper.WaitForBodyText);
            actions.Add("waitForChecked", CommandHelper.WaitForChecked);
            actions.Add("waitForCssCount", CommandHelper.WaitForCssCount);
            actions.Add("waitForNotCssCount", CommandHelper.WaitForNotCssCount);
            actions.Add("waitForXpathCount", CommandHelper.WaitForXpathCount);
            

            actions.Add("waitForLocation", CommandHelper.WaitForLocation);
            actions.Add("waitForNotLocation", CommandHelper.WaitForNotLocation);
            actions.Add("waitForNotAlert", CommandHelper.WaitForNotAlertConfirmation);
            actions.Add("waitForNotConfirmation", CommandHelper.WaitForNotAlertConfirmation);
            actions.Add("waitForNotAttribute", CommandHelper.WaitForNotAttribute);
            actions.Add("waitForNotBodyText", CommandHelper.WaitForNotBodyText);
            actions.Add("waitForNotChecked", CommandHelper.WaitForNotChecked);
            actions.Add("waitForNotText", CommandHelper.WaitForNotText);
            actions.Add("waitForNotTitle", CommandHelper.WaitForNotTitle);
            actions.Add("waitForTitle", CommandHelper.WaitForTitle);
            actions.Add("waitForNotValue", CommandHelper.WaitForNotValue);
            actions.Add("waitForValue", CommandHelper.WaitForValue);
            actions.Add("waitForNotVisible", CommandHelper.WaitForNotVisible);
            actions.Add("waitForVisible", CommandHelper.WaitForVisible);
            
            actions.Add("waitForNotXpathCount", CommandHelper.WaitForNotXpathCount);
            actions.Add("waitForText", CommandHelper.WaitForText);
            actions.Add("waitForTextNotPresent", CommandHelper.WaitForTextNotPresent);
            actions.Add("waitForTextPresent", CommandHelper.WaitForTextPresent);

            actions.Add("devXComboSelect", CommandHelper.DevXComboSelect);
            actions.Add("devXComboSelectAndWait", CommandHelper.DevXComboSelect);
            actions.Add("devXComboSelectItem", CommandHelper.DevXComboSelectItem);
            actions.Add("devXComboSelectItemAndWait", CommandHelper.DevXComboSelectItem);
            actions.Add("closeNotification", CommandHelper.CloseNotification);
            actions.Add("closeNotificationAndWait", CommandHelper.CloseNotification);
        }

        public static TestCommand Parse(string command, string argument1, string argument2)
        {
            string key = String.Format("{0}|{1}|{2}", command.ToLowerInvariant().Trim(), argument1.ToLowerInvariant().Trim(), argument2.ToLowerInvariant().Trim());
            lock (syncLock)
            {
                if (!commands.ContainsKey(key))
                {
                    TestCommand obj = new TestCommand(command.Trim(), argument1.Trim(), argument2.Trim());
                    commands.Add(key, obj);
                }
            }
            return commands[key];
        }

        public static bool IsVerify(TestCommand command)
        {
            if (command == null)
            {
                throw new ArgumentNullException("command");
            }

            return command.Command.StartsWith("verify", StringComparison.InvariantCultureIgnoreCase);
        }

        public static bool IsAssert(TestCommand command)
        {
            if (command == null)
            {
                throw new ArgumentNullException("command");
            }

            return command.Command.StartsWith("assert", StringComparison.InvariantCultureIgnoreCase);
        }

        public static bool IsWait(TestCommand command)
        {
            if (command == null)
            {
                throw new ArgumentNullException("command");
            }

            return command.Command.EndsWith("AndWait", StringComparison.InvariantCultureIgnoreCase);
        }

        private static string PreparePattern(string pattern, bool strict = true)
        {
            // TODO: Ver http://release.seleniumhq.org/selenium-core/1.0/reference.html#patterns
            string ret, format;
            if (strict)
            {
                format = "^{0}$";
            }
            else
            {
                format = "{0}";
            }
            // ret = String.Format(format, Regex.Escape(pattern.Replace("**", "*")).Replace("*", "[\\s\\S]*").Replace("?", "[\\s\\S]"));
            ret = String.Format(format, pattern.Replace("*", "[\\s\\S]*").Replace("?", "[\\s\\S]"));
            return ret;
        }

        private static void AssertText(string test, string value, bool assertPositive = true, bool strict = true)
        {
            Action<bool> isFn;
            Action<string, string> equalFn;
            if (assertPositive)
            {
                isFn = Assert.IsTrue;
                equalFn = Assert.AreEqual;
            }
            else
            {
                isFn = Assert.IsFalse;
                equalFn = Assert.AreNotEqual;
            }

            if (test.Contains("*") || test.Contains("?")
             || test.StartsWith("glob:")
             || test.StartsWith("regexp:")
             || test.StartsWith("regexpi:")
             || !test.StartsWith("exact:"))
            {
                string pattern = PreparePattern(test, strict);
                RegexOptions flag = RegexOptions.Compiled;
                if (test.StartsWith("regexpi:"))
                {
                    flag |= RegexOptions.IgnoreCase;
                }
                isFn(Regex.IsMatch(value, pattern, flag));
            }
            else
            {
                equalFn(test, value);
            }
        }

        #region Commands

        #region AssertVerify

        private static void AssertVerifyAlertConfirmation(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            string arg1 = StateHelper.Parse(cmd.Argument1, ste);

            AssertText(arg1, drv.CloseAlertAndGetItsText(AcceptNextAlert));
        }

        private static void AssertVerifyAlertConfirmationNotPresent(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            Assert.IsFalse(drv.IsAlertPresent());
        }

        private static void AssertVerifyAlertConfirmationPresent(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            Assert.IsTrue(drv.IsAlertPresent());
        }

        private static void AssertVerifyAttribute(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            string arg1 = StateHelper.Parse(cmd.Argument1, ste),
                   arg2 = StateHelper.Parse(cmd.Argument2, ste);
            string[] aux = arg1.Split(new char[] { '@' }, StringSplitOptions.RemoveEmptyEntries);
            string loc = aux[0],
                   attribute = aux[1];
            By locator = LocatorHelper.Parse(loc);
            IWebElement element = drv.FindElementDeep(locator);
            string attrValue = element.GetAttribute(attribute);
            AssertText(attrValue, arg2);
        }

        private static void AssertVerifyBodyText(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            string arg1 = StateHelper.Parse(cmd.Argument1, ste),
                   arg2 = StateHelper.Parse(cmd.Argument2, ste);

            By locator = By.TagName("BODY");
            IWebElement element = drv.FindElement(locator);
            AssertText(arg2, element.Text);
        }

        private static void AssertVerifyChecked(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            string arg1 = StateHelper.Parse(cmd.Argument1, ste),
                   arg2 = StateHelper.Parse(cmd.Argument2, ste);

            By locator = LocatorHelper.Parse(arg1);
            IWebElement element = drv.FindElementDeep(locator);
            Assert.IsTrue(element.Selected);
        }

        private static void AssertVerifyCssCount(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            string arg1 = StateHelper.Parse(cmd.Argument1, ste),
                   arg2 = StateHelper.Parse(cmd.Argument2, ste);
            By locator = By.CssSelector(arg1);
            int cnt = int.Parse(arg2);
            IList<IWebElement> elements = drv.FindElements(locator);

            Assert.AreEqual(cnt, elements.Count);
        }

        private static void AssertVerifyElementNotPresent(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            string arg1 = StateHelper.Parse(cmd.Argument1, ste),
                   arg2 = StateHelper.Parse(cmd.Argument2, ste);

            By locator = LocatorHelper.Parse(arg1);

            Assert.IsFalse(drv.IsElementPresent(locator));
        }

        private static void AssertVerifyElementPresent(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            string arg1 = StateHelper.Parse(cmd.Argument1, ste),
                   arg2 = StateHelper.Parse(cmd.Argument2, ste);

            By locator = LocatorHelper.Parse(arg1);

            Assert.IsTrue(drv.IsElementPresent(locator));
        }

        private static void AssertVerifyLocation(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            string arg1 = StateHelper.Parse(cmd.Argument1, ste),
                   arg2 = StateHelper.Parse(cmd.Argument2, ste);

            AssertText(arg1, drv.Url);
        }

        private static void AssertVerifyNotLocation(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            string arg1 = StateHelper.Parse(cmd.Argument1, ste),
                   arg2 = StateHelper.Parse(cmd.Argument2, ste);

            AssertText(arg1, drv.Url, false);
        }

        private static void AssertVerifyNotAlertConfirmation(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            string arg1 = StateHelper.Parse(cmd.Argument1, ste);

            AssertText(arg1, drv.CloseAlertAndGetItsText(AcceptNextAlert), false);
        }

        private static void AssertVerifyNotAttribute(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            string arg1 = StateHelper.Parse(cmd.Argument1, ste),
               arg2 = StateHelper.Parse(cmd.Argument2, ste);
            string[] aux = arg1.Split(new char[] { '@' }, StringSplitOptions.RemoveEmptyEntries);
            string loc = aux[0],
                    attribute = aux[1];
            By locator = LocatorHelper.Parse(loc);
            IWebElement element = drv.FindElementDeep(locator);
            string attrValue = element.GetAttribute(attribute);
            AssertText(attrValue, arg2, false);
        }

        private static void AssertVerifyNotBodyText(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            string arg1 = StateHelper.Parse(cmd.Argument1, ste),
                   arg2 = StateHelper.Parse(cmd.Argument2, ste);

            By locator = By.TagName("BODY");
            IWebElement element = drv.FindElement(locator);

            AssertText(arg1, element.Text, false);
        }

        private static void AssertVerifyNotChecked(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            string arg1 = StateHelper.Parse(cmd.Argument1, ste),
                   arg2 = StateHelper.Parse(cmd.Argument2, ste);

            By locator = LocatorHelper.Parse(arg1);
            IWebElement element = drv.FindElementDeep(locator);
            Assert.IsFalse(element.Selected);
        }

        private static void AssertVerifyNotConfirmation(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            string arg1 = StateHelper.Parse(cmd.Argument1, ste);

            AssertText(arg1, drv.CloseAlertAndGetItsText(AcceptNextAlert), false);
        }

        private static void AssertVerifyNotCssCount(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            string arg1 = StateHelper.Parse(cmd.Argument1, ste),
                   arg2 = StateHelper.Parse(cmd.Argument2, ste);
            By locator = By.CssSelector(arg1);
            int cnt = int.Parse(arg2);
            IList<IWebElement> elements = drv.FindElements(locator);
            Assert.AreNotEqual(cnt, elements.Count);
        }

        private static void AssertVerifyNotVisible(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            string arg1 = StateHelper.Parse(cmd.Argument1, ste),
                    arg2 = StateHelper.Parse(cmd.Argument2, ste);

            By locator = LocatorHelper.Parse(arg1);
            IWebElement element = drv.FindElementDeep(locator);
            Assert.IsFalse(element.Displayed);
        }

        private static void AssertVerifyText(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            string arg1 = StateHelper.Parse(cmd.Argument1, ste),
                    arg2 = StateHelper.Parse(cmd.Argument2, ste);

            By locator = LocatorHelper.Parse(arg1);
            IWebElement element = drv.FindElementDeep(locator);
            AssertText(arg2, element.Text);
        }

        private static void AssertVerifyNotText(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            string arg1 = StateHelper.Parse(cmd.Argument1, ste),
                    arg2 = StateHelper.Parse(cmd.Argument2, ste);

            By locator = LocatorHelper.Parse(arg1);
            IWebElement element = drv.FindElementDeep(locator);
            AssertText(arg2, element.Text, false);
        }

        private static void AssertVerifyTextNotPresent(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            string arg1 = StateHelper.Parse(cmd.Argument1, ste),
               arg2 = StateHelper.Parse(cmd.Argument2, ste),
               value = arg1;

            if (String.IsNullOrEmpty(arg1))
            {
                value = arg2;
            }

            By locator = By.CssSelector("BODY");
            IWebElement element = drv.FindElement(locator);
            AssertText(String.Format("*{0}*", value), element.Text, false);
        }

        private static void AssertVerifyTextPresent(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            string arg1 = StateHelper.Parse(cmd.Argument1, ste),
                   arg2 = StateHelper.Parse(cmd.Argument2, ste),
                   value = arg1;

            if (String.IsNullOrEmpty(arg1))
            {
                value = arg2;
            }

            By locator = By.CssSelector("BODY");
            IWebElement element = drv.FindElement(locator);
            AssertText(String.Format("*{0}*", value), element.Text);
        }

        private static void AssertVerifyValue(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            string arg1 = StateHelper.Parse(cmd.Argument1, ste),
                   arg2 = StateHelper.Parse(cmd.Argument2, ste);
            By locator = LocatorHelper.Parse(arg1);
            IWebElement element = drv.FindElementDeep(locator);
            string value = element.GetAttribute("value");
            AssertText(arg2, value);
        }

        private static void AssertVerifyNotValue(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            string arg1 = StateHelper.Parse(cmd.Argument1, ste),
                   arg2 = StateHelper.Parse(cmd.Argument2, ste);
            By locator = LocatorHelper.Parse(arg1);
            IWebElement element = drv.FindElementDeep(locator);
            string value = element.GetAttribute("value");
            AssertText(arg2, value, false);
        }

        private static void AssertVerifyTitle(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            string arg1 = StateHelper.Parse(cmd.Argument1, ste),
               arg2 = StateHelper.Parse(cmd.Argument2, ste);

            AssertText(arg1, drv.Title);
        }

        private static void AssertVerifyNotTitle(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            string arg1 = StateHelper.Parse(cmd.Argument1, ste);

            AssertText(arg1, drv.Title, false);
        }

        private static void AssertVerifyVisible(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            string arg1 = StateHelper.Parse(cmd.Argument1, ste),
                   arg2 = StateHelper.Parse(cmd.Argument2, ste);

            By locator = LocatorHelper.Parse(arg1);
            IWebElement element = drv.FindElementDeep(locator);
            Assert.IsTrue(element.Displayed);
        }

        private static void AssertVerifyXpathCount(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            string arg1 = StateHelper.Parse(cmd.Argument1, ste),
                   arg2 = StateHelper.Parse(cmd.Argument2, ste);
            int value = int.Parse(arg2);

            By locator = LocatorHelper.Parse(arg1);
            IList<IWebElement> elements = drv.FindElements(locator);
            Assert.AreEqual(value, elements.Count);
        }

        private static void AssertVerifyNotXpathCount(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            string arg1 = StateHelper.Parse(cmd.Argument1, ste),
                   arg2 = StateHelper.Parse(cmd.Argument2, ste);
            int value = int.Parse(arg2);

            By locator = LocatorHelper.Parse(arg1);
            IList<IWebElement> elements = drv.FindElements(locator);
            Assert.AreNotEqual(value, elements.Count);
        }

        #endregion AssertVerify

        private static void Open(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            string arg1 = StateHelper.Parse(cmd.Argument1, ste),
                   targetUrl = String.Format(CultureInfo.InvariantCulture, "{0}{1}", url, arg1);
            drv.Navigate().GoToUrl(targetUrl);
        }

        private static void Clear(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            string arg1 = StateHelper.Parse(cmd.Argument1, ste);
            By locator = LocatorHelper.Parse(arg1);
            IWebElement element = drv.FindElementDeep(locator);
            if (HighlightElement)
            {
                element.HighlightElement();
            }
            element.Clear();
        }

        private static void SendKeys(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            string arg1 = StateHelper.Parse(cmd.Argument1, ste),
                   arg2 = StateHelper.Parse(cmd.Argument2, ste);
            By locator = LocatorHelper.Parse(arg1);
            IWebElement element = drv.FindElementDeep(locator);
            if (HighlightElement)
            {
                element.HighlightElement();
            }
            element.SendKeys(arg2);
        }

        private static void Type(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            string arg1 = StateHelper.Parse(cmd.Argument1, ste),
                   arg2 = StateHelper.Parse(cmd.Argument2, ste);
            By locator = LocatorHelper.Parse(arg1);
            IWebElement element = drv.FindElementDeep(locator);
            if (HighlightElement)
            {
                element.HighlightElement();
            }
            try
            {
                element.Clear();
            }
            catch
            {
            }
            element.SendKeys(arg2);
        }

        private static void Submit(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            string arg1 = StateHelper.Parse(cmd.Argument1, ste);
            // string arg2 = StateHelper.Parse(cmd.Argument2, ste);
            By locator = LocatorHelper.Parse(arg1);
            IWebElement element = drv.FindElementDeep(locator);
            if (HighlightElement)
            {
                element.HighlightElement();
            }
            element.Submit();
        }

        private static void DevXComboSelectItem(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            string arg1 = StateHelper.Parse(cmd.Argument1, ste),
                                   arg2 = StateHelper.Parse(cmd.Argument2, ste),
                                   ctl = arg1.Substring(0, arg1.Length - 2),
                                   imgLocatorId = String.Format("{0}_B-1Img", ctl),
                                   itemLocatorId = String.Format("{0}_DDD_L_LBI{1}T0", ctl, arg2);
            By ctlLocator = LocatorHelper.Parse(arg1),
                imgLocator = LocatorHelper.Parse(imgLocatorId),
               itemLocator = LocatorHelper.Parse(itemLocatorId);

            if (HighlightElement)
            {
                drv.FindElementDeep(ctlLocator).HighlightElement();
            }

            drv.FindElementDeep(drv.WaitForElementPresent(imgLocator)).Click();
            drv.FindElementDeep(drv.WaitForVisible(itemLocator)).Click();
        }

        private static void DevXComboSelect(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            string arg1 = StateHelper.Parse(cmd.Argument1, ste),
                    ctl = arg1.Substring(0, arg1.Length - 2),
                    imgLocatorId = String.Format("{0}_B-1Img", ctl),
                    itemLocatorId = String.Format("{0}_DDD_L_LBI0T0", ctl);
            By ctlLocator = LocatorHelper.Parse(arg1),
                imgLocator = LocatorHelper.Parse(imgLocatorId),
               itemLocator = LocatorHelper.Parse(itemLocatorId);

            if (HighlightElement)
            {
                drv.FindElementDeep(ctlLocator).HighlightElement();
            }

            drv.FindElementDeep(drv.WaitForElementPresent(imgLocator)).Click();
            drv.FindElementDeep(drv.WaitForVisible(itemLocator)).Click();
        }

        //private static void WaitForVisible(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        //{
        //    if (cmd == null)
        //    {
        //        throw new ArgumentNullException("cmd");
        //    }

        //    if (drv == null)
        //    {
        //        throw new ArgumentNullException("drv");
        //    }

        //    if (url == null)
        //    {
        //        throw new ArgumentNullException("url");
        //    }

        //    if (ste == null)
        //    {
        //        throw new ArgumentNullException("ste");
        //    }

        //    string arg1 = StateHelper.Parse(cmd.Argument1, ste),
        //           arg2 = StateHelper.Parse(cmd.Argument2, ste);
        //    By locator = LocatorHelper.Parse(arg1);
        //    drv.WaitForVisible(locator);
        //}

        private static void Focus(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            string arg1 = StateHelper.Parse(cmd.Argument1, ste),
                   arg2 = StateHelper.Parse(cmd.Argument2, ste);
            By locator = LocatorHelper.Parse(arg1);
            IWebElement element = drv.FindElementDeep(locator);
            if (HighlightElement)
            {
                element.HighlightElement();
            }
            // drv.FindElementOnPage(locator);
            element.Focus();

        }

        private static void Click(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            string arg1 = StateHelper.Parse(cmd.Argument1, ste),
                   arg2 = StateHelper.Parse(cmd.Argument2, ste);
            By locator = LocatorHelper.Parse(arg1);
            IWebElement element = drv.FindElementDeep(locator);
            if (HighlightElement)
            {
                element.HighlightElement();
            }
            element.Focus();
            element.Click();
        }

        private static void ChooseCancelOnNextConfirmation(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            acceptNextAlert = false;

        }

        private static void ChooseOkOnNextConfirmation(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            acceptNextAlert = true;

        }

        private static void GoBack(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            drv.Navigate().Back();

        }

        private static void Refresh(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            drv.Navigate().Refresh();

        }

        private static void Check(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            string arg1 = StateHelper.Parse(cmd.Argument1, ste),
                           arg2 = StateHelper.Parse(cmd.Argument2, ste);
            By locator = LocatorHelper.Parse(arg1);
            IWebElement element = drv.FindElementDeep(locator);

            if (!element.Selected)
            {

                if (HighlightElement)
                {
                    element.HighlightElement();
                }
                element.Focus();
                element.Click();
            }

        }

        private static void UnCheck(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            string arg1 = StateHelper.Parse(cmd.Argument1, ste),
                           arg2 = StateHelper.Parse(cmd.Argument2, ste);
            By locator = LocatorHelper.Parse(arg1);
            IWebElement element = drv.FindElementDeep(locator);

            if (element.Selected)
            {

                if (HighlightElement)
                {
                    element.HighlightElement();
                }
                element.Focus();
                element.Click();
            }
        }

        private static void Echo(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            string arg1 = StateHelper.Parse(cmd.Argument1, ste),
                   arg2 = StateHelper.Parse(cmd.Argument2, ste);
            Console.WriteLine(arg1);
        }

        private static void Dummy(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
        }

        private static void CloseNotification(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            string arg1 = StateHelper.Parse(cmd.Argument1, ste),
                   arg2 = StateHelper.Parse(cmd.Argument2, ste);

            By locator = LocatorHelper.Parse(arg1);
            IWebElement element;
            try
            {
                element = drv.FindElementDeep(locator);
            }
            catch (Exception)
            {
                drv.WaitForElementPresent(locator);
            }
            element = drv.FindElementDeep(locator);
            element.Focus();
            element.Click();
        }

        #region Store

        private static void Store(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            string arg1 = StateHelper.Parse(cmd.Argument1, ste),
                   arg2 = StateHelper.Parse(cmd.Argument2, ste);
            ste[arg2] = arg1;
        }

        private static void StoreAlertConfirmation(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            string arg1 = StateHelper.Parse(cmd.Argument1, ste);
            ste[arg1] = drv.CloseAlertAndGetItsText(AcceptNextAlert);
        }

        private static void StoreAlertConfirmationPresent(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            string arg1 = StateHelper.Parse(cmd.Argument1, ste);
            ste[arg1] = drv.IsAlertPresent();
        }

        private static void StoreAttribute(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            string arg1 = StateHelper.Parse(cmd.Argument1, ste),
                   arg2 = StateHelper.Parse(cmd.Argument2, ste);
            string[] aux = arg1.Split(new char[] { '@' }, StringSplitOptions.RemoveEmptyEntries);
            string loc = aux[0],
                   attribute = aux[1];
            By locator = LocatorHelper.Parse(loc);
            IWebElement element = drv.FindElementDeep(locator);
            string attrValue = element.GetAttribute(attribute);
            ste[arg2] = attrValue;
        }

        private static void StoreBodyText(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            string arg1 = StateHelper.Parse(cmd.Argument1, ste),
                   arg2 = StateHelper.Parse(cmd.Argument2, ste);

            By locator = By.TagName("BODY");
            IWebElement element = drv.FindElementDeep(locator);
            ste[arg2] = element.Text;
        }

        private static void StoreChecked(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            string arg1 = StateHelper.Parse(cmd.Argument1, ste),
                   arg2 = StateHelper.Parse(cmd.Argument2, ste);

            By locator = LocatorHelper.Parse(arg1);
            IWebElement element = drv.FindElementDeep(locator);
            ste[arg2] = element.Selected;
        }

        private static void StoreCssCount(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            string arg1 = StateHelper.Parse(cmd.Argument1, ste),
                   arg2 = StateHelper.Parse(cmd.Argument2, ste);
            By locator = By.CssSelector(arg1);
            IList<IWebElement> elements = drv.FindElements(locator);

            ste[arg2] = elements.Count;
        }

        private static void StoreElementPresent(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            string arg1 = StateHelper.Parse(cmd.Argument1, ste),
                   arg2 = StateHelper.Parse(cmd.Argument2, ste);

            By locator = LocatorHelper.Parse(arg1);
            ste[arg2] = drv.IsElementPresent(locator);
        }

        private static void StoreLocation(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            string arg1 = StateHelper.Parse(cmd.Argument1, ste);

            ste[arg1] = drv.Url;
        }

        private static void StoreText(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            string arg1 = StateHelper.Parse(cmd.Argument1, ste),
                    arg2 = StateHelper.Parse(cmd.Argument2, ste);

            By locator = LocatorHelper.Parse(arg1);
            IWebElement element = drv.FindElementDeep(locator);
            ste[arg2] = element.Text;
        }

        private static void StoreTitle(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            string arg1 = StateHelper.Parse(cmd.Argument1, ste);

            ste[arg1] = drv.Title;
        }

        private static void StoreValue(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            string arg1 = StateHelper.Parse(cmd.Argument1, ste),
                   arg2 = StateHelper.Parse(cmd.Argument2, ste);
            By locator = LocatorHelper.Parse(arg1);
            IWebElement element = drv.FindElementDeep(locator);
            string value = element.GetAttribute("value");
            ste[arg2] = value;
        }

        private static void StoreVisible(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            string arg1 = StateHelper.Parse(cmd.Argument1, ste),
                   arg2 = StateHelper.Parse(cmd.Argument2, ste);
            By locator = LocatorHelper.Parse(arg1);
            IWebElement element = drv.FindElementDeep(locator);
            ste[arg2] = element.Displayed;
        }

        private static void StoreXpathCount(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            string arg1 = StateHelper.Parse(cmd.Argument1, ste),
                   arg2 = StateHelper.Parse(cmd.Argument2, ste);


            By locator = LocatorHelper.Parse(arg1);
            IList<IWebElement> elements = drv.FindElements(locator);
            ste[arg2] = elements.Count;
        }

        private static void StoreToday(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            string arg1 = StateHelper.Parse(cmd.Argument1, ste),
                   arg2 = StateHelper.Parse(cmd.Argument2, ste);
            ste[arg2] = System.DateTime.Today.ToString(arg1);
        }

        private static void StoreTomorrow(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            string arg1 = StateHelper.Parse(cmd.Argument1, ste),
                   arg2 = StateHelper.Parse(cmd.Argument2, ste);
            ste[arg2] = System.DateTime.Today.AddDays(1).ToString(arg1);
        }

        private static void StoreNextWeek(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            string arg1 = StateHelper.Parse(cmd.Argument1, ste),
                   arg2 = StateHelper.Parse(cmd.Argument2, ste);
            ste[arg2] = System.DateTime.Today.AddDays(7).ToString(arg1);
        }

        #endregion Store

        #region Wait

        private static void Pause(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            string arg1 = StateHelper.Parse(cmd.Argument1, ste),
                   arg2 = StateHelper.Parse(cmd.Argument2, ste);
            int pause;

            if (!int.TryParse(arg1, out pause))
            {
                if (!int.TryParse(arg2, out pause))
                {
                    throw new ArgumentException("pause");
                }
            }
            Thread.Sleep(pause);
        }

        private static void WaitForPageToLoad(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            string arg1 = StateHelper.Parse(cmd.Argument1, ste),
                   arg2 = StateHelper.Parse(cmd.Argument2, ste),
                   value = arg1 ?? arg2;

            uint pause;

            if (!uint.TryParse(arg1, out pause))
            {
                if (!uint.TryParse(arg2, out pause))
                {
                    pause = 60;
                }
            }

            drv.WaitForPageToLoad(pause);
        }

        private static void WaitForElementPresent(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            string arg1 = StateHelper.Parse(cmd.Argument1, ste);
            By locator = LocatorHelper.Parse(arg1);
            // drv.WaitForElementPresent(locator);
            drv.WaitForElement(locator, true);
        }

        private static void WaitForElementNotPresent(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            string arg1 = StateHelper.Parse(cmd.Argument1, ste),
                   arg2 = StateHelper.Parse(cmd.Argument2, ste);

            By locator = LocatorHelper.Parse(arg1);
            drv.WaitForElement(locator, false);
        }

        private static void WaitForAlertConfirmation(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            string arg1 = StateHelper.Parse(cmd.Argument1, ste);

            drv.WaitForCondition(() =>
            {
                AssertText(arg1, drv.CloseAlertAndGetItsText(AcceptNextAlert));
                return true;
            });
        }

        private static void WaitForAlertConfirmationNotPresent(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            drv.WaitForCondition(() =>
            {
                AssertVerifyAlertConfirmationNotPresent(cmd, drv, url, ste);
                return true;
            });
        }

        private static void WaitForAlertConfirmationPresent(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            drv.WaitForCondition(() =>
            {
                AssertVerifyAlertConfirmationPresent(cmd, drv, url, ste);
                return true;
            });
        }

        private static void WaitForAttribute(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            drv.WaitForCondition(() =>
            {
                AssertVerifyAttribute(cmd, drv, url, ste);
                return true;
            });
        }

        private static void WaitForNotAttribute(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            drv.WaitForCondition(() =>
            {
                AssertVerifyNotAttribute(cmd, drv, url, ste);
                return true;
            });
        }

        private static void WaitForBodyText(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            drv.WaitForCondition(() =>
            {
                AssertVerifyBodyText(cmd, drv, url, ste);
                return true;
            });
        }

        private static void WaitForNotBodyText(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            drv.WaitForCondition(() =>
            {
                AssertVerifyNotBodyText(cmd, drv, url, ste);
                return true;
            });
        }

        private static void WaitForChecked(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            drv.WaitForCondition(() =>
            {
                AssertVerifyChecked(cmd, drv, url, ste);
                return true;
            });
        }

        private static void WaitForNotChecked(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            drv.WaitForCondition(() =>
            {
                AssertVerifyNotChecked(cmd, drv, url, ste);
                return true;
            });
        }

        private static void WaitForCssCount(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            drv.WaitForCondition(() =>
            {
                AssertVerifyCssCount(cmd, drv, url, ste);
                return true;
            });
        }

        private static void WaitForNotCssCount(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            drv.WaitForCondition(() =>
            {
                AssertVerifyNotCssCount(cmd, drv, url, ste);
                return true;
            });
        }

        private static void WaitForXpathCount(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            drv.WaitForCondition(() =>
            {
                AssertVerifyCssCount(cmd, drv, url, ste);
                return true;
            });
        }        

        private static void WaitForLocation(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            drv.WaitForCondition(() =>
            {
                AssertVerifyLocation(cmd, drv, url, ste);
                return true;
            });
        }

        private static void WaitForNotLocation(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            drv.WaitForCondition(() =>
            {
                AssertVerifyNotLocation(cmd, drv, url, ste);
                return true;
            });
        }

        private static void WaitForNotText(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            drv.WaitForCondition(() =>
            {
                AssertVerifyNotText(cmd, drv, url, ste);
                return true;
            });
        }

        private static void WaitForTitle(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            drv.WaitForCondition(() =>
            {
                AssertVerifyTitle(cmd, drv, url, ste);
                return true;
            });
        }

        private static void WaitForNotTitle(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            drv.WaitForCondition(() =>
            {
                AssertVerifyNotTitle(cmd, drv, url, ste);
                return true;
            });
        }

        private static void WaitForNotValue(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            drv.WaitForCondition(() =>
            {
                AssertVerifyNotValue(cmd, drv, url, ste);
                return true;
            });
        }

        private static void WaitForValue(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            drv.WaitForCondition(() =>
            {
                AssertVerifyValue(cmd, drv, url, ste);
                return true;
            });
        }

        private static void WaitForNotVisible(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            drv.WaitForCondition(() =>
            {
                AssertVerifyNotVisible(cmd, drv, url, ste);
                return true;
            });
        }

        private static void WaitForVisible(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            drv.WaitForCondition(() =>
            {
                AssertVerifyVisible(cmd, drv, url, ste);
                return true;
            });
        }        

        private static void WaitForNotXpathCount(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            drv.WaitForCondition(() =>
            {
                AssertVerifyNotXpathCount(cmd, drv, url, ste);
                return true;
            });
        }

        private static void WaitForText(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            drv.WaitForCondition(() =>
            {
                AssertVerifyText(cmd, drv, url, ste);
                return true;
            });
        }

        private static void WaitForTextNotPresent(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            drv.WaitForCondition(() =>
            {
                AssertVerifyTextNotPresent(cmd, drv, url, ste);
                return true;
            });
        }

        private static void WaitForTextPresent(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            drv.WaitForCondition(() =>
            {
                AssertVerifyTextPresent(cmd, drv, url, ste);
                return true;
            });
        }

        private static void WaitForNotAlertConfirmation(TestCommand cmd, IWebDriver drv, string url, IDictionary<string, object> ste)
        {
            if (cmd == null)
            {
                throw new ArgumentNullException("cmd");
            }

            if (drv == null)
            {
                throw new ArgumentNullException("drv");
            }

            if (url == null)
            {
                throw new ArgumentNullException("url");
            }

            if (ste == null)
            {
                throw new ArgumentNullException("ste");
            }

            drv.WaitForCondition(() =>
            {
                AssertVerifyNotAlertConfirmation(cmd, drv, url, ste);
                return true;
            });
        }

        #endregion

        #endregion Commands

        public static Action<TestCommand, IWebDriver, string, IDictionary<string, object>> Parse(TestCommand command)
        {
            Action<TestCommand, IWebDriver, string, IDictionary<string, object>> action = null;
            Action<TestCommand, IWebDriver, string, IDictionary<string, object>> actionToRun = null;

            if (actions.ContainsKey(command.Command))
            {
                action = Open;

                bool andWait = CommandHelper.IsWait(command);

                action = actions[command.Command];

                if (andWait)
                {
                    actionToRun = (cmd, drv, url, ste) =>
                    {
                        action(cmd, drv, url, ste);
                        drv.WaitForPageToLoad();
                    };
                }
                else
                {
                    actionToRun = action;
                }
            }
            else
            {
                throw new NotImplementedException(String.Format("Command: {0} is not implemented", command.Command));
            }
            return actionToRun;
        }
    }
}