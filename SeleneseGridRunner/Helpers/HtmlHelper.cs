namespace SeleneseGridRunner
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Net;
    using HtmlAgilityPack;

    /// <summary>
    /// PArsea los Test Case o las Test Suite.
    /// </summary>
    public static class HtmlHelper
    {
        const string searchSuiteTestCases = "//table[@id='suiteTable']//a[@href]";
        const string searchTestCaseBaseUrl = "//link[@rel='selenium.base' and @href]";
        const string searchTestCaseSteps = "//tbody/tr";
        const string searhTestCaseStepCommandParts = "td";

        /// <summary>
        /// 
        /// </summary>
        /// <param name="testFiles"></param>
        /// <returns></returns>
        public static IList<TestSuite> Parse(IList<string> testFiles)
        {
            List<TestSuite> suites = new List<TestSuite>();
            TestSuite suite;
            foreach (string testFilePath in testFiles)
            {
                HtmlDocument doc = new HtmlDocument();
                doc.Load(testFilePath, true);
                // Detectar TestSuite o TestCase
                if (IsSuite(doc))
                {
                    suite = GetSuite(doc, testFilePath);
                }
                else
                {
                    TestCase testCase = GetTestCase(doc, Path.GetFileNameWithoutExtension(testFilePath), testFilePath);
                    List<TestCase> testCases = new List<TestCase>();
                    testCases.Add(testCase);
                    suite = new TestSuite(testCases.AsReadOnly(), testFilePath);
                }
                suites.Add(suite);
            }
            return suites.AsReadOnly();
        }

        /// <summary>
        /// Detecta si es una suite o un testCase.
        /// </summary>
        /// <param name="doc"></param>
        /// <returns></returns>
        private static bool IsSuite(HtmlDocument doc)
        {
            bool ret = true;
            try
            {
                HtmlNode el = doc.GetElementbyId("suiteTable");
                ret = el != null;
            }
            catch
            {
                ret = false;
            }
            return ret;
        }

        private static TestSuite GetSuite(HtmlDocument suiteDoc, string suitePath)
        {
            List<TestCase> testCases = new List<TestCase>();
            HtmlNodeCollection suiteTestCases = suiteDoc.DocumentNode.SelectNodes(searchSuiteTestCases);
            foreach (HtmlNode testCaseLink in suiteTestCases)
            {
                string testCaseName = WebUtility.HtmlDecode(testCaseLink.InnerText),
                       testCaseFile = testCaseLink.Attributes["href"].Value,
                       testCaseFilePath = Path.Combine(Path.GetDirectoryName(suitePath), testCaseFile);

                HtmlDocument testCaseDoc = new HtmlDocument();
                testCaseDoc.Load(testCaseFilePath, true);
                TestCase testCase = GetTestCase(testCaseDoc, testCaseName, testCaseFilePath);
                testCases.Add(testCase);
            }
            TestSuite suite = new TestSuite(testCases.AsReadOnly(), suitePath);
            return suite;
        }

        private static TestCase GetTestCase(HtmlDocument testCaseDoc, string testCaseName, string testCaseFilePath)
        {
            List<TestCommand> commands = new List<TestCommand>();
            string baseUrl = testCaseDoc.DocumentNode.SelectSingleNode(searchTestCaseBaseUrl).Attributes["href"].Value;
            foreach (HtmlNode step in testCaseDoc.DocumentNode.SelectNodes(searchTestCaseSteps))
            {
                HtmlNodeCollection stepCommand = step.SelectNodes(searhTestCaseStepCommandParts);
                string command = WebUtility.HtmlDecode(stepCommand[0].InnerText),
                       arg1 = WebUtility.HtmlDecode(stepCommand[1].InnerText),
                       arg2 = WebUtility.HtmlDecode(stepCommand[2].InnerText);
                if (!String.IsNullOrWhiteSpace(command))
                {
                    TestCommand cmd = CommandHelper.Parse(command, arg1, arg2);
                    commands.Add(cmd);
                }
            }
            TestCase testCase = new TestCase(testCaseName, testCaseFilePath, baseUrl, commands.AsReadOnly());
            return testCase;
        }
    }
}
