namespace SeleneseGridRunner
{
    using System;
    using OpenQA.Selenium;
    using System.Collections.Generic;

    internal static class LocatorHelper
    {

        private readonly static IDictionary<string, string> tags = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase) { { "a", "a" }, 
                                                                                                                                                 { "abbr", "abbr" }, 
                                                                                                                                                 { "address", "address" }, 
                                                                                                                                                 { "area", "area" }, 
                                                                                                                                                 { "article", "article" }, 
                                                                                                                                                 { "aside", "aside" }, 
                                                                                                                                                 { "audio", "audio" }, 
                                                                                                                                                 { "b", "b" }, 
                                                                                                                                                 { "base", "base" }, 
                                                                                                                                                 { "bdi", "bdi" }, 
                                                                                                                                                 { "bdo", "bdo" }, 
                                                                                                                                                 { "blockquote", "blockquote" }, 
                                                                                                                                                 { "body", "body" }, 
                                                                                                                                                 { "br", "br" }, 
                                                                                                                                                 { "button", "button" }, 
                                                                                                                                                 { "canvas", "canvas" }, 
                                                                                                                                                 { "caption", "caption" }, 
                                                                                                                                                 { "cite", "cite" }, 
                                                                                                                                                 { "code", "code" }, 
                                                                                                                                                 { "col", "col" }, 
                                                                                                                                                 { "colgroup", "colgroup" }, 
                                                                                                                                                 { "command", "command" }, 
                                                                                                                                                 { "data", "data" }, 
                                                                                                                                                 { "datagrid", "datagrid" }, 
                                                                                                                                                 { "datalist", "datalist" }, 
                                                                                                                                                 { "dd", "dd" }, 
                                                                                                                                                 { "del", "del" }, 
                                                                                                                                                 { "details", "details" }, 
                                                                                                                                                 { "dfn", "dfn" }, 
                                                                                                                                                 { "div", "div" }, 
                                                                                                                                                 { "dl", "dl" }, 
                                                                                                                                                 { "dt", "dt" }, 
                                                                                                                                                 { "em", "em" }, 
                                                                                                                                                 { "embed", "embed" }, 
                                                                                                                                                 { "eventsource", "eventsource" }, 
                                                                                                                                                 { "fieldset", "fieldset" }, 
                                                                                                                                                 { "figcaption", "figcaption" }, 
                                                                                                                                                 { "figure", "figure" }, 
                                                                                                                                                 { "footer", "footer" }, 
                                                                                                                                                 { "form", "form" }, 
                                                                                                                                                 { "h1", "h1" }, 
                                                                                                                                                 { "h2", "h2" }, 
                                                                                                                                                 { "h3", "h3" }, 
                                                                                                                                                 { "h4", "h4" }, 
                                                                                                                                                 { "h5", "h5" }, 
                                                                                                                                                 { "h6", "h6" }, 
                                                                                                                                                 { "head", "head" }, 
                                                                                                                                                 { "header", "header" }, 
                                                                                                                                                 { "hgroup", "hgroup" }, 
                                                                                                                                                 { "hr", "hr" }, 
                                                                                                                                                 { "html", "html" }, 
                                                                                                                                                 { "i", "i" }, 
                                                                                                                                                 { "iframe", "iframe" }, 
                                                                                                                                                 { "img", "img" }, 
                                                                                                                                                 { "input", "input" }, 
                                                                                                                                                 { "ins", "ins" }, 
                                                                                                                                                 { "kbd", "kbd" }, 
                                                                                                                                                 { "keygen", "keygen" }, 
                                                                                                                                                 { "label", "label" }, 
                                                                                                                                                 { "legend", "legend" }, 
                                                                                                                                                 { "li", "li" }, 
                                                                                                                                                 { "link", "link" }, 
                                                                                                                                                 { "mark", "mark" }, 
                                                                                                                                                 { "map", "map" }, 
                                                                                                                                                 { "menu", "menu" }, 
                                                                                                                                                 { "meta", "meta" }, 
                                                                                                                                                 { "meter", "meter" }, 
                                                                                                                                                 { "nav", "nav" }, 
                                                                                                                                                 { "noscript", "noscript" }, 
                                                                                                                                                 { "object", "object" }, 
                                                                                                                                                 { "ol", "ol" }, 
                                                                                                                                                 { "optgroup", "optgroup" }, 
                                                                                                                                                 { "option", "option" }, 
                                                                                                                                                 { "output", "output" }, 
                                                                                                                                                 { "p", "p" }, 
                                                                                                                                                 { "param", "param" }, 
                                                                                                                                                 { "pre", "pre" }, 
                                                                                                                                                 { "progress", "progress" }, 
                                                                                                                                                 { "q", "q" }, 
                                                                                                                                                 { "ruby", "ruby" }, 
                                                                                                                                                 { "rp", "rp" }, 
                                                                                                                                                 { "rt", "rt" }, 
                                                                                                                                                 { "s", "s" }, 
                                                                                                                                                 { "samp", "samp" }, 
                                                                                                                                                 { "script", "script" }, 
                                                                                                                                                 { "section", "section" }, 
                                                                                                                                                 { "select", "select" }, 
                                                                                                                                                 { "small", "small" }, 
                                                                                                                                                 { "source", "source" }, 
                                                                                                                                                 { "span", "span" }, 
                                                                                                                                                 { "strong", "strong" }, 
                                                                                                                                                 { "style", "style" }, 
                                                                                                                                                 { "sub", "sub" }, 
                                                                                                                                                 { "summary", "summary" }, 
                                                                                                                                                 { "sup", "sup" }, 
                                                                                                                                                 { "table", "table" }, 
                                                                                                                                                 { "tbody", "tbody" }, 
                                                                                                                                                 { "td", "td" }, 
                                                                                                                                                 { "textarea", "textarea" }, 
                                                                                                                                                 { "tfoot", "tfoot" }, 
                                                                                                                                                 { "th", "th" }, 
                                                                                                                                                 { "thead", "thead" }, 
                                                                                                                                                 { "time", "time" }, 
                                                                                                                                                 { "title", "title" }, 
                                                                                                                                                 { "tr", "tr" }, 
                                                                                                                                                 { "track", "track" }, 
                                                                                                                                                 { "u", "u" }, 
                                                                                                                                                 { "ul", "ul" }, 
                                                                                                                                                 { "var", "var" }, 
                                                                                                                                                 { "video", "video" }, 
                                                                                                                                                 { "wbr", "wbr" } };


        /// <summary>
        /// 
        /// </summary>
        /// <param name="locator"></param>
        /// <returns></returns>
        /// <seealso cref="http://release.seleniumhq.org/selenium-core/0.8.0/reference.html"/>
        public static By Parse(string locator)
        {

            By ret = null;
            if (!String.IsNullOrWhiteSpace(locator))
            {
                if (locator.StartsWith("id=", StringComparison.InvariantCultureIgnoreCase) || locator.StartsWith("identifier=", StringComparison.InvariantCultureIgnoreCase))
                {
                    ret = By.Id(locator.Substring(3));
                }
                else if (locator.StartsWith("css=", StringComparison.InvariantCultureIgnoreCase))
                {
                    ret = By.CssSelector(locator.Substring(4));
                }
                else if (locator.StartsWith("link=", StringComparison.InvariantCultureIgnoreCase))
                {
                    ret = By.LinkText(locator.Substring(5));
                }
                else if (locator.StartsWith("name=", StringComparison.InvariantCultureIgnoreCase))
                {
                    ret = By.Name(locator.Substring(5));
                }
                else if (locator.StartsWith("xpath=", StringComparison.InvariantCultureIgnoreCase))
                {
                    ret = By.XPath(locator.Substring(6));
                }
                else if (locator.StartsWith("//", StringComparison.InvariantCultureIgnoreCase))
                {
                    ret = By.XPath(locator);
                }
                else if (tags.ContainsKey(locator))
                {
                    ret = By.TagName(locator);
                }
                else
                {
                    ret = By.Id(locator);
                }

                // By.ClassName
                // By.PartialLinkText

            }
            return ret;
        }
    }
}
