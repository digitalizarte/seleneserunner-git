namespace SeleneseGridRunner
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Text.RegularExpressions;

    public static class StateHelper
    {
        // const string pattern = @"(?:\${)(?<varName>[\w\d]+)(?:})";
        const string pattern = @"\${(?<varName>[\w\d]+)}";
        const RegexOptions regExpFlag = RegexOptions.Compiled | RegexOptions.IgnoreCase;

        public static string Parse(string argument, IDictionary<string, object> state)
        {
            string ret;
            Match match = Regex.Match(argument, pattern, regExpFlag);
            // if (Regex.IsMatch(argument, pattern, regExpFlag))
            if (match.Success)
            {
                // string varName = Regex.Match(argument, pattern, regExpFlag).Groups["varName"].Value;
                string varName = match.Groups["varName"].Value;
                object value = state[varName];
                // ret = argument.Replace(String.Format("${{{0}}}", varName), Convert.ToString(value, CultureInfo.InvariantCulture));
                ret = argument.Replace(match.Value, Convert.ToString(value, CultureInfo.InvariantCulture));
            }
            else
            {
                ret = argument;
            }
            return ret;
        }
    }
}
