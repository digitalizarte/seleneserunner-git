﻿using System;
namespace SeleneseGridRunner
{
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.IO;

    public class SelenseExecutorConfiguration
    {
        private readonly ConcurrentBag<string> screenShots = new ConcurrentBag<string>();
        private readonly IList<TestSuite> suites;
        private readonly IList<string> browsers;
        private readonly string baseUrl;
        private readonly string hubUrl;

        public SelenseExecutorConfiguration(IList<TestSuite> suites, IList<string> browsers, string baseUrl, string hubUrl)
        {
            this.suites = suites;
            this.browsers = browsers;
            this.baseUrl = baseUrl;
            this.hubUrl = hubUrl;
        }

        public ConcurrentBag<string> ScreenShots
        {
            get
            {
                return screenShots;
            }
        }

        public IList<TestSuite> Suites
        {
            get
            {
                return suites;
            }
        }

        public IList<string> Browsers
        {
            get
            {
                return browsers;
            }
        }

        public string BaseUrl
        {
            get
            {
                return baseUrl;
            }
        }

        public string HubUrl
        {
            get
            {
                return hubUrl;
            }
        }

        public bool TakeScreenShot
        {
            get;
            set;
        }

        public bool TakeScreenShotAll
        {
            get;
            set;
        }

        public string TakeScreenShotDirectory
        {
            get;
            set;
        }

        public bool IsDebug
        {
            get;
            set;
        }

        public int Speed
        {
            get;
            set;
        }

        public TextWriter Out
        {
            get;
            set;
        }


        public bool IsLocal
        {
            get;
            set;
        }

        public bool UseSharedVars
        {
            get;
            set;
        }

        public bool CaptureJsErrors
        {
            get;
            set;
        }
    }
}
