using System.Threading;
using System.Diagnostics;
namespace SeleneseGridRunner
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Drawing.Imaging;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using NUnit.Framework;
    using OpenQA.Selenium;
    using OpenQA.Selenium.Remote;
    using System.Text;
    using OpenQA.Selenium.Support.Events;

    /// <summary>
    /// Ejecuta las Suites de test, los TestCase y los Comandos Selenese.
    /// </summary>
    public static class SeleneseExecutorHelper
    {
        private readonly static object syncLock = new object();

        /// <summary>
        /// Ejecuta las suite.s
        /// </summary>
        /// <param name="config"></param>
        /// <returns></returns>
        public static IList<TestSuiteResult> Execute(SelenseExecutorConfiguration config)
        {
            List<Task> tasks = new List<Task>();
            ConcurrentBag<TestSuiteResult> results = new ConcurrentBag<TestSuiteResult>();

            foreach (var suite in config.Suites)
            {
                Debug(config, "Suite: {0}", Path.GetFileName(suite.FilePath));
                foreach (string browser in config.Browsers)
                {
                    Task task = Task.Factory.StartNew(() =>
                    {
                        Stopwatch sw = Stopwatch.StartNew();
                        try
                        {
                            using (IWebDriver webDriver = GetDriver(browser, config))
                            {
                                Debug(config, "Suite: {0} Browser: {1}", Path.GetFileName(suite.FilePath), browser);
                                sw.Stop();
                                TestSuiteResult result = Execute(suite, config, webDriver, browser);
                                results.Add(result);
                                webDriver.Close();
                            }
                        }
                        catch (Exception ex)
                        {
                            sw.Stop();
                            TestSuiteResult result = new TestSuiteResult(suite, null, config.BaseUrl, browser, TestStatus.Failed, sw.Elapsed, ex);
                            results.Add(result);
                        }
                    });
                    tasks.Add(task);
                }
                Task.WaitAll(tasks.ToArray());
                tasks.Clear();
            }
            return results.ToList().AsReadOnly();
        }

        private static void Debug(SelenseExecutorConfiguration config, string format, params object[] args)
        {
            if (config.IsDebug && config.Out != null)
            {
                lock (syncLock)
                {
                    config.Out.WriteLine(format, args);
                }
            }
        }

        /// <summary>
        /// Ejecuta la suite.
        /// </summary>
        /// <param name="suite"></param>
        /// <param name="config"></param>
        /// <param name="webDriver"></param>
        /// <param name="browser"></param>
        /// <returns></returns>
        public static TestSuiteResult Execute(TestSuite suite,
                                                SelenseExecutorConfiguration config,
                                                IWebDriver webDriver,
                                                string browser)
        {
            TestStatus status = TestStatus.Passed;
            bool ok = true;
            List<TestCaseResult> list = new List<TestCaseResult>();
            string suiteName = Path.GetFileNameWithoutExtension(suite.FilePath).Replace(" ", "_").Replace("__", "_");
            Stopwatch sw = Stopwatch.StartNew();
            foreach (TestCase testCase in suite.TestCases)
            {
                string suiteFilePath = Path.GetFileNameWithoutExtension(suite.FilePath);
                string testCaseFilePath = Path.GetFileNameWithoutExtension(testCase.FilePath);
                if (suiteFilePath != testCaseFilePath)
                {
                    Debug(config, "Suite: {0} Browser: {1} TestCase: {2} ({3})", suiteFilePath, browser, testCase.Name, testCaseFilePath);
                }
                else
                {
                    Debug(config, "TestCase: {0} ({1}) Browser: {2}", testCase.Name, testCaseFilePath, browser);
                }
                TestCaseResult result;
                if (config.UseSharedVars)
                {
                    IDictionary<string, object> state = new Dictionary<string, object>();
                    result = Execute(testCase, config, webDriver, suiteName, browser, state);
                }
                else
                {
                    result = Execute(testCase, config, webDriver, suiteName, browser);
                }

                if (ok)
                {
                    ok = result.Status == TestStatus.Passed;
                    if (!ok)
                    {
                        status = result.Status;
                    }
                }
                list.Add(result);
            }
            sw.Stop();
            return new TestSuiteResult(suite, list.AsReadOnly(), config.BaseUrl, browser, status, sw.Elapsed);
        }

        /// <summary>
        /// Ejecuta el TestCase. 
        /// </summary>
        /// <param name="testCase"></param>
        /// <param name="config"></param>
        /// <param name="webDriver"></param>
        /// <param name="suiteName"></param>
        /// <param name="browser"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        private static TestCaseResult Execute(TestCase testCase,
                                                SelenseExecutorConfiguration config,
                                                IWebDriver webDriver,
                                                string suiteName,
                                                string browser,
                                                IDictionary<string, object> state = null)
        {
            Stopwatch sw = Stopwatch.StartNew();
            TestStatus status = TestStatus.Passed;
            List<TestCommandResult> list = new List<TestCommandResult>();
            bool ok = true;
            string testCaseName = testCase.Name.Replace(" ", "_").Replace("__", "_");
            string fileName;
            StringBuilder verificationErrors = new StringBuilder();
            Type testStatusType = typeof(TestStatus);
            int configSpeed = config.Speed;
            try
            {
                IWebDriver driver;
                List<object> errors = new List<object>();
                if (config.CaptureJsErrors || true)
                {
                    driver = new EventFiringWebDriver(webDriver);
                    EventHandler<WebDriverNavigationEventArgs> captureJsErros = (s, e) =>
                    {
                        IWebDriver drv = (s as IWebDriver);
                        if (drv.Url != "about:blank")
                        {
                            errors.AddRange(drv.RecursiveCaptureJsErros());
                            drv.RecursiveRemoveErrorHandler();
                        }
                    };

                    EventHandler<WebDriverNavigationEventArgs> initCaptureJs = (s, e) =>
                    {
                        IWebDriver drv = (s as IWebDriver);
                        if (drv.Url != "about:blank")
                        {
                            drv.WaitForPageToLoad();
                            drv.RecursiveInjectErrorHandler();
                        }
                    };

                    (driver as EventFiringWebDriver).AddNavigatingHandler(captureJsErros)
                                                    .AddNavigatedHandler(initCaptureJs)
                                                    .AddNavigatedBackHandler(captureJsErros)
                                                    .AddNavigatingBackHandler(initCaptureJs)
                                                    .AddNavigatingForwardHandler(captureJsErros)
                                                    .AddNavigatedForwardHandler(initCaptureJs);
                }
                else
                {
                    driver = webDriver;
                }

                // IOptions manage = webDriver.Manage();
                IOptions manage = driver.Manage();
                manage.Window.Maximize();
                manage.Cookies.DeleteAllCookies();
                string baseUrl = !String.IsNullOrWhiteSpace(config.BaseUrl) ? config.BaseUrl : testCase.BaseUrl;
                if (state == null)
                {
                    state = new Dictionary<string, object>();
                }
                CommandHelper.HighlightElement = config.IsDebug;
                CommandHelper.AcceptNextAlert = true;

                for (int i = 0, l = testCase.TestCommands.Count; i < l; i++)
                {
                    Stopwatch swCmd = Stopwatch.StartNew();
                    TestCommandResult result;
                    TestCommand command = testCase.TestCommands[i];
                    if (ok)
                    {
                        string testCaseFilePath = Path.GetFileNameWithoutExtension(testCase.FilePath);
                        if (suiteName != testCaseFilePath)
                        {
                            Debug(config, "Suite: {0} Browser: {1} TestCase: {2} Command: {3} {4}", suiteName, browser, testCase.Name, i.ToString().PadLeft(5, '0'), command);
                        }
                        else
                        {
                            Debug(config, "TestCase: {0} Browser: {1} Command: {2} {3}", testCase.Name, browser, i.ToString().PadLeft(5, '0'), command);
                        }

                        // result = Execute(command, webDriver, baseUrl, state, verificationErrors);
                        result = Execute(command, driver, baseUrl, state, verificationErrors);
                        ok = result.Status == TestStatus.Passed;
                        if (!ok)
                        {
                            status = result.Status;
                        }

                        if (config.CaptureJsErrors)
                        {
                            // Verifica si se produjeron errores de Js.
                            // errors.AddRange(webDriver.RecursiveCaptureJsErros());
                            errors.AddRange(driver.RecursiveCaptureJsErros());
                        }
                    }
                    else
                    {
                        swCmd.Stop();
                        result = new TestCommandResult(command, baseUrl, swCmd.Elapsed);
                        result.Status = TestStatus.Skipped;
                    }

                    try
                    {
                        if (config.TakeScreenShotAll)
                        {
                            fileName = String.Format(CultureInfo.InvariantCulture,
                                                       "{0:yyyyMMdd}_{1}_{2}_{3}_{4}_{5}_{6}",
                                                       DateTime.Today,
                                                       browser,
                                                       suiteName,
                                                       testCaseName,
                                                       i,
                                                       command.Command,
                                                       Enum.GetName(testStatusType, result.Status));
                            // TakeScreenShot(webDriver, config, fileName);
                            TakeScreenShot(driver, config, fileName);
                        }
                    }
                    catch
                    {
                    }
                    list.Add(result);

                    if (command.Command == "setSpeed")
                    {
                        configSpeed = (int)Convert.ToInt32(command.Argument1) / 4;
                    }

                    if (ok && configSpeed > 0)
                    {
                        Thread.Sleep(Math.Max(1000, config.Speed));
                    }
                }

                if (errors.Any())
                {
                    System.Diagnostics.Debug.Assert(false);
                }
            }
            catch (Exception)
            {
                ok = false;
                status = TestStatus.Failed;
            }

            if (ok && verificationErrors.Length != 0)
            {
                status = TestStatus.Inconclusive;
            }

            try
            {
                if (config.TakeScreenShot || config.TakeScreenShotAll)
                {
                    fileName = String.Format(CultureInfo.InvariantCulture,
                                               "{0:yyyyMMdd}_{1}_{2}_{3}_{4}",
                                               DateTime.Today,
                                               browser,
                                               suiteName,
                                               testCaseName,
                                               Enum.GetName(testStatusType, status));
                    TakeScreenShot(webDriver, config, fileName);
                }
            }
            catch
            {
            }
            sw.Stop();
            return new TestCaseResult(testCase, list.AsReadOnly(), status, sw.Elapsed);

        }


        /// <summary>
        /// Ejecuta el comando Selenese.
        /// </summary>
        /// <param name="command"></param>
        /// <param name="webDriver"></param>
        /// <param name="baseUrl"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        private static TestCommandResult Execute(TestCommand command,
                                                IWebDriver webDriver,
                                                string baseUrl,
                                                IDictionary<string, object> state,
                                                StringBuilder verificationErrors)
        {
            Stopwatch sw = Stopwatch.StartNew();
            TestCommandResult result = null;
            try
            {
                Action<TestCommand, IWebDriver, string, IDictionary<string, object>> action = CommandHelper.Parse(command);
                action(command, webDriver, baseUrl, state);
                sw.Stop();
                result = new TestCommandResult(command, baseUrl, sw.Elapsed);
                result.Status = TestStatus.Passed;
            }
            catch (AssertionException aex)
            {
                sw.Stop();
                result = new TestCommandResult(command, baseUrl, sw.Elapsed);
                if (CommandHelper.IsVerify(command))
                {
                    result.Status = TestStatus.Passed;
                    verificationErrors.AppendLine(aex.Message);
                }
                else
                {
                    result.Status = TestStatus.Failed;
                }
                result.Exception = aex;
            }
            catch (Exception ex)
            {
                sw.Stop();
                result = new TestCommandResult(command, baseUrl, sw.Elapsed);
                result.Status = TestStatus.Failed;
                result.Exception = ex;
            }
            return result;
        }

        /// <summary>
        /// Obtiene el driver seg�n el navegador.
        /// </summary>
        /// <param name="browser"></param>
        /// <param name="hubUrl"></param>
        /// <returns></returns>
        private static IWebDriver GetDriver(string browser, SelenseExecutorConfiguration config)
        {
            IWebDriver driver = null;
            if (config.IsLocal)
            {
                if (browser.StartsWith("android", StringComparison.InvariantCultureIgnoreCase))
                {
                    driver = new OpenQA.Selenium.Android.AndroidDriver();
                }
                else if (browser.StartsWith("chrome", StringComparison.InvariantCultureIgnoreCase))
                {
                    driver = new OpenQA.Selenium.Chrome.ChromeDriver();
                }
                else if (browser.StartsWith("firefox", StringComparison.InvariantCultureIgnoreCase))
                {
                    driver = new OpenQA.Selenium.Firefox.FirefoxDriver();
                }
                else if (browser.StartsWith("ie", StringComparison.InvariantCultureIgnoreCase) || browser.StartsWith("internetexplorer", StringComparison.InvariantCultureIgnoreCase))
                {
                    driver = new OpenQA.Selenium.IE.InternetExplorerDriver();
                }
                else if (browser.StartsWith("safari", StringComparison.InvariantCultureIgnoreCase))
                {
                    driver = new OpenQA.Selenium.Safari.SafariDriver();
                }
                else if (browser.StartsWith("htmlunit", StringComparison.InvariantCultureIgnoreCase))
                {
                    throw new NotSupportedException("htmlunit");
                }
                else if (browser.StartsWith("htmlunitwithjavascript", StringComparison.InvariantCultureIgnoreCase))
                {
                    throw new NotSupportedException("htmlunitwithjavascript");
                }
                else if (browser.StartsWith("ipad", StringComparison.InvariantCultureIgnoreCase))
                {
                    throw new NotSupportedException("ipad");
                }
                else if (browser.StartsWith("iphone", StringComparison.InvariantCultureIgnoreCase))
                {
                    throw new NotSupportedException("iphone");
                }
                else if (browser.StartsWith("opera", StringComparison.InvariantCultureIgnoreCase))
                {
                    throw new NotSupportedException("opera");
                }
                else if (browser.StartsWith("phantomjs", StringComparison.InvariantCultureIgnoreCase))
                {
                    throw new NotSupportedException("phantomjs");
                }
            }
            else
            {
                ICapabilities capabilitie = null;
                if (browser.StartsWith("android", StringComparison.InvariantCultureIgnoreCase))
                {
                    capabilitie = DesiredCapabilities.Android();
                }
                else if (browser.StartsWith("chrome", StringComparison.InvariantCultureIgnoreCase))
                {
                    capabilitie = DesiredCapabilities.Chrome();
                }
                else if (browser.StartsWith("firefox", StringComparison.InvariantCultureIgnoreCase))
                {
                    capabilitie = DesiredCapabilities.Firefox();
                }
                else if (browser.StartsWith("htmlunit", StringComparison.InvariantCultureIgnoreCase))
                {
                    capabilitie = DesiredCapabilities.HtmlUnit();
                }
                else if (browser.StartsWith("htmlunitwithjavascript", StringComparison.InvariantCultureIgnoreCase))
                {
                    capabilitie = DesiredCapabilities.HtmlUnitWithJavaScript();
                }
                else if (browser.StartsWith("ie", StringComparison.InvariantCultureIgnoreCase) || browser.StartsWith("internetexplorer", StringComparison.InvariantCultureIgnoreCase))
                {
                    capabilitie = DesiredCapabilities.InternetExplorer();
                }
                else if (browser.StartsWith("ipad", StringComparison.InvariantCultureIgnoreCase))
                {
                    capabilitie = DesiredCapabilities.IPad();
                }
                else if (browser.StartsWith("iphone", StringComparison.InvariantCultureIgnoreCase))
                {
                    capabilitie = DesiredCapabilities.IPhone();
                }
                else if (browser.StartsWith("opera", StringComparison.InvariantCultureIgnoreCase))
                {
                    capabilitie = DesiredCapabilities.Opera();
                }
                else if (browser.StartsWith("phantomjs", StringComparison.InvariantCultureIgnoreCase))
                {
                    capabilitie = DesiredCapabilities.PhantomJS();
                }
                else if (browser.StartsWith("safari", StringComparison.InvariantCultureIgnoreCase))
                {
                    capabilitie = DesiredCapabilities.Safari();
                }
                Uri uri = new Uri(config.HubUrl);
                driver = new ScreenShotRemoteWebDriver(uri, capabilitie);
            }
            return driver;
        }

        private static void TakeScreenShot(IWebDriver webDriver, SelenseExecutorConfiguration config, string fileName)
        {
            string filePath = Path.ChangeExtension(Path.Combine(config.TakeScreenShotDirectory ?? Path.GetTempPath(), fileName), "png");
            (webDriver as ITakesScreenshot).GetScreenshot().SaveAsFile(filePath, ImageFormat.Png);
            config.ScreenShots.Add(fileName);
        }
    }
}