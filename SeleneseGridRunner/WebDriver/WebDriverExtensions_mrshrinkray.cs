﻿namespace OpenQA.Selenium
{

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using OpenQA.Selenium;
    using OpenQA.Selenium.Support.UI;
    using System.Threading;

    /// <summary>
    /// A set of CSS and form based extension methods for <see cref="IWebDriver"/>.
    /// </summary>
    /// <see cref="https://bitbucket.org/mrshrinkray/seleniumextensions"/>
    public static partial class WebDriverExtensions
    {
        /// <summary>
        /// Clicks a button that has the given value.
        /// </summary>
        /// <param name="buttonValue">The button's value (input[value=])</param>
        /// <param name="webdriver">A <see cref="IWebDriver"/> instance.</param>
        /// <exception cref="OpenQA.Selenium.NoSuchElementException">No element was found.</exception>
        public static void ClickButtonWithValue(this IWebDriver webdriver, string buttonValue)
        {
            string exp = String.Format("input[value='{0}']", buttonValue);
            By locator = By.CssSelector(exp);
            webdriver.FindElement(locator).Click();
        }

        /// <summary>
        /// Clicks a button with the id ending with the provided value.
        /// </summary>
        /// <param name="idEndsWith">A CSS id.</param>
        /// <param name="webdriver">A <see cref="IWebDriver"/> instance.</param>
        /// <exception cref="OpenQA.Selenium.NoSuchElementException">No element was found.</exception>
        public static void ClickButtonWithId(this IWebDriver webdriver, string idEndsWith)
        {
            string exp = String.Format("input[id$='{0}']", idEndsWith);
            By locator = By.CssSelector(exp);
            webdriver.FindElement(locator).Click();
        }

        /// <summary>
        /// Selects an item from a drop down box using the given CSS id and the itemIndex.
        /// </summary>
        /// <param name="selector">A valid CSS selector.</param>
        /// <param name="itemIndex">A zero-based index that determines which drop down box to target from the CSS selector (assuming
        /// the CSS selector returns more than one drop down box).</param>
        /// <param name="webdriver">A <see cref="IWebDriver"/> instance.</param>
        /// <exception cref="OpenQA.Selenium.NoSuchElementException">No element was found.</exception>
        public static void SelectItemInList(this IWebDriver webdriver, string selector, int itemIndex)
        {
            By locator = By.CssSelector(selector);
            IWebElement element = webdriver.FindElement(locator);
            SelectElement selElement = new SelectElement(element);
            selElement.SelectByIndex(itemIndex);
        }

        /// <summary>
        /// Selects an item from the nth drop down box (based on the elementIndex argument), using the given CSS id and the itemIndex.
        /// </summary>
        /// <param name="selector">A valid CSS selector.</param>
        /// <param name="itemIndex">A zero-based index that determines which drop down box to target from the CSS selector (assuming
        /// the CSS selector returns more than one drop down box).</param>
        /// <param name="elementIndex">The element in the drop down list to select.</param>
        /// <param name="webdriver">A <see cref="IWebDriver"/> instance.</param>
        /// <exception cref="OpenQA.Selenium.NoSuchElementException">No element was found.</exception>
        public static void SelectItemInList(this IWebDriver webdriver, string selector, int itemIndex, int elementIndex)
        {
            By locator = By.CssSelector(selector);
            IList<IWebElement> elements = webdriver.FindElements(locator);
            IWebElement element = elements[elementIndex];
            SelectElement selElement = new SelectElement(element);
            selElement.SelectByIndex(itemIndex);
        }

        /// <summary>
        /// Returns the number of elements that match the given CSS selector.
        /// </summary>
        /// <param name="selector">A valid CSS selector.</param>
        /// <param name="webdriver">A <see cref="IWebDriver"/> instance.</param>
        /// <returns>The number of elements found.</returns>
        /// <exception cref="OpenQA.Selenium.NoSuchElementException">No element was found.</exception>
        public static int ElementCount(this IWebDriver webdriver, string selector)
        {
            By locator = By.CssSelector(selector);
            IList<IWebElement> elements = webdriver.FindElements(locator);
            return elements.Count;
        }

        /// <summary>
        /// Gets the selected index from a drop down box using the given CSS selector.
        /// </summary>
        /// <param name="selector">A valid CSS selector.</param>
        /// <param name="webdriver">A <see cref="IWebDriver"/> instance.</param>
        /// <returns>The index of the selected item in the drop down box</returns>
        /// <exception cref="OpenQA.Selenium.NoSuchElementException">No element was found.</exception>
        public static int SelectedIndex(this IWebDriver webdriver, string selector)
        {
            By locator = By.CssSelector(selector);
            IWebElement element = webdriver.FindElement(locator);
            SelectElement selElement = new SelectElement(element);
            for (int i = 0, l = selElement.Options.Count; i < l; i++)
            {
                if (selElement.Options[i].Selected)
                {
                    return i;
                }
            }

            return -1;
        }

        /// <summary>
        /// Gets the selected index from the nth drop down box (based on the elementIndex argument), using the given CSS selector.
        /// </summary>
        /// <param name="selector">A valid CSS selector.</param>
        /// <param name="itemIndex">A zero-based index that determines which drop down box to target from the CSS selector (assuming
        /// the CSS selector returns more than one drop down box).</param>
        /// <param name="webdriver">A <see cref="IWebDriver"/> instance.</param>
        /// <returns>The index of the selected item in the drop down box</returns>
        /// <exception cref="OpenQA.Selenium.NoSuchElementException">No element was found.</exception>
        public static int SelectedIndex(this IWebDriver webdriver, string selector, int itemIndex)
        {
            By locator = By.CssSelector(selector);
            IList<IWebElement> elements = webdriver.FindElements(locator);
            IWebElement element = elements[itemIndex];
            SelectElement selElement = new SelectElement(element);
            for (int i = 0, l = selElement.Options.Count; i < l; i++)
            {
                if (selElement.Options[i].Selected)
                {
                    return i;
                }
            }
            return -1;
        }

        /// <summary>
        /// Gets the selected value from a drop down box using the given CSS selector.
        /// </summary>
        /// <param name="selector">A valid CSS selector.</param>
        /// <param name="webdriver">A <see cref="IWebDriver"/> instance.</param>
        /// <returns>The value of the selected item.</returns>
        /// <exception cref="OpenQA.Selenium.NoSuchElementException">No element was found.</exception>
        public static string SelectedItemValue(this IWebDriver webdriver, string selector)
        {
            By locator = By.CssSelector(selector);
            SelectElement element = (SelectElement)webdriver.FindElement(locator);
            return element.SelectedOption.GetAttribute("value");
        }

        /// <summary>
        /// Clicks a link with the text provided. This is case sensitive and searches using an Xpath contains() search.
        /// </summary>
        /// <param name="linkContainsText">The link text to search for.</param>
        /// <param name="webdriver">A <see cref="IWebDriver"/> instance.</param>
        /// <exception cref="OpenQA.Selenium.NoSuchElementException">No element was found.</exception>
        public static void ClickLinkWithText(this IWebDriver webdriver, string linkContainsText)
        {
            string exp = String.Format("//a[contains(text(),'{0}')]", linkContainsText);
            By locator = By.XPath(exp);
            IWebElement element = webdriver.FindElement(locator);
            element.Click();
        }

        /// <summary>
        /// Clicks a link with the id ending with the provided value.
        /// </summary>
        /// <param name="idEndsWith">A CSS id.</param>
        /// <param name="webdriver">A <see cref="IWebDriver"/> instance.</param>
        /// <exception cref="OpenQA.Selenium.NoSuchElementException">No element was found.</exception>
        public static void ClickLinkWithId(this IWebDriver webdriver, string idEndsWith)
        {
            string exp = String.Format("a[id$='{0}']", idEndsWith);
            By locator = By.CssSelector(exp);
            IWebElement element = webdriver.FindElement(locator);
            element.Click();
        }

        /// <summary>
        /// Clicks an element using the given CSS selector.
        /// </summary>
        /// <param name="selector">A valid CSS selector.</param>
        /// <param name="webdriver">A <see cref="IWebDriver"/> instance.</param>
        /// <exception cref="OpenQA.Selenium.NoSuchElementException">No element was found.</exception>
        public static void Click(this IWebDriver webdriver, string selector)
        {
            By locator = By.CssSelector(selector);
            IWebElement element = webdriver.FindElement(locator);
            element.Click();
        }

        /// <summary>
        /// Clicks an element using the given CSS selector.
        /// </summary>
        /// <param name="selector">A valid CSS selector.</param>
        /// <param name="itemIndex">A zero-based index that determines which element to target from the CSS selector (assuming
        /// the CSS selector returns more than one element).</param>
        /// <param name="webdriver">A <see cref="IWebDriver"/> instance.</param>
        /// <exception cref="OpenQA.Selenium.NoSuchElementException">No element was found.</exception>
        public static void Click(this IWebDriver webdriver, string selector, int itemIndex)
        {
            By locator = By.CssSelector(selector);
            IList<IWebElement> elements = webdriver.FindElements(locator);
            IWebElement element = elements[itemIndex];
            element.Click();
        }

        /// <summary>
        /// Gets an input element with the id ending with the provided value.
        /// </summary>
        /// <param name="idEndsWith">A CSS id.</param>
        /// <param name="webdriver">A <see cref="IWebDriver"/> instance.</param>
        /// <returns>An <see cref="IWebElement"/> for the item matched.</returns>
        /// <exception cref="OpenQA.Selenium.NoSuchElementException">No element was found.</exception>
        public static IWebElement InputWithId(this IWebDriver webdriver, string idEndsWith)
        {
            string exp = String.Format("input[id$='{0}']", idEndsWith);
            By locator = By.CssSelector(exp);
            return webdriver.FindElement(locator);
        }

        /// <summary>
        /// Gets an element's value with the id ending with the provided value.
        /// </summary>
        /// <param name="idEndsWith">A CSS id.</param>
        /// <param name="webdriver">A <see cref="IWebDriver"/> instance.</param>
        /// <returns>The element's value.</returns>
        /// <exception cref="OpenQA.Selenium.NoSuchElementException">No element was found.</exception>
        public static string ElementValueWithId(this IWebDriver webdriver, string idEndsWith)
        {
            string exp = String.Format("input[id$='{0}']", idEndsWith);
            By locator = By.CssSelector(exp);
            IWebElement element = webdriver.FindElement(locator);
            return element.GetAttribute("value");
        }

        /// <summary>
        /// Gets an element's value using the given CSS selector.
        /// </summary>
        /// <param name="selector">A valid CSS selector.</param>
        /// <param name="webdriver">A <see cref="IWebDriver"/> instance.</param>
        /// <returns>The element's value.</returns>
        /// <exception cref="OpenQA.Selenium.NoSuchElementException">No element was found.</exception>
        public static string ElementValue(this IWebDriver webdriver, string selector)
        {
            By locator = By.CssSelector(selector);
            IWebElement element = webdriver.FindElement(locator);
            return element.GetAttribute("value");
        }

        /// <summary>
        /// Gets an element's value using the given CSS selector.
        /// </summary>
        /// <param name="selector">A valid CSS selector.</param>
        /// <param name="itemIndex">A zero-based index that determines which element to target from the CSS selector (assuming
        /// the CSS selector returns more than one element).</param>
        /// <param name="webdriver">A <see cref="IWebDriver"/> instance.</param>
        /// <returns>The element's value.</returns>
        /// <exception cref="OpenQA.Selenium.NoSuchElementException">No element was found.</exception>
        public static string ElementValue(this IWebDriver webdriver, string selector, int itemIndex)
        {
            By locator = By.CssSelector(selector);
            IList<IWebElement> elements = webdriver.FindElements(locator);
            IWebElement element = elements[itemIndex];
            return element.GetAttribute("value");
        }

        /// <summary>
        /// Gets an element's text using the given CSS selector.
        /// </summary>
        /// <param name="selector">A valid CSS selector.</param>
        /// <param name="itemIndex">A zero-based index that determines which element to target from the CSS selector (assuming
        /// the CSS selector returns more than one element).</param>
        /// <param name="webdriver">A <see cref="IWebDriver"/> instance.</param>
        /// <returns>The element's text.</returns>
        /// <exception cref="OpenQA.Selenium.NoSuchElementException">No element was found.</exception>
        public static string ElementText(this IWebDriver webdriver, string selector, int itemIndex)
        {
            By locator = By.CssSelector(selector);
            IList<IWebElement> elements = webdriver.FindElements(locator);
            IWebElement element = elements[itemIndex];
            return element.Text;
        }

        /// <summary>
        /// Return true if the checkbox with the id ending with the provided value is checked/selected.
        /// </summary>
        /// <param name="idEndsWith">A CSS id.</param>
        /// <param name="webdriver">A <see cref="IWebDriver"/> instance.</param>
        /// <returns>True if the checkbox is checked.</returns>
        /// <exception cref="OpenQA.Selenium.NoSuchElementException">No element was found.</exception>
        public static bool IsCheckboxChecked(this IWebDriver webdriver, string idEndsWith)
        {
            string exp = String.Format("input[id$='{0}']", idEndsWith);
            By locator = By.CssSelector(exp);
            IWebElement element = webdriver.FindElement(locator);
            return element.Selected;
        }

        /// <summary>
        /// Clicks the checkbox with the id ending with the provided value.
        /// </summary>
        /// <param name="idEndsWith">A CSS id.</param>
        /// <param name="webdriver">A <see cref="IWebDriver"/> instance.</param>
        /// <exception cref="OpenQA.Selenium.NoSuchElementException">No element was found.</exception>
        public static void ClickCheckbox(this IWebDriver webdriver, string idEndsWith)
        {
            string exp = String.Format("input[id$='{0}']", idEndsWith);
            By locator = By.CssSelector(exp);
            IWebElement element = webdriver.FindElement(locator);
            element.Click();
        }

        /// <summary>
        /// Sets an element's (an input field) value to the provided text by using SendKeys().
        /// </summary>
        /// <param name="value">The text to type.</param>
        /// <param name="element">A <see cref="IWebElement"/> instance.</param>
        /// <exception cref="OpenQA.Selenium.NoSuchElementException">No element was found.</exception>
        public static void SetValue(this IWebElement element, string value)
        {
            element.SendKeys(value);
        }

        /// <summary>
        /// Sets an element's (an input field) value to the provided text, using the given CSS selector and using SendKeys().
        /// </summary>
        /// <param name="selector">A valid CSS selector.</param>
        /// <param name="value">The text to type.</param>
        /// <param name="webdriver">A <see cref="IWebDriver"/> instance.</param>
        /// <exception cref="OpenQA.Selenium.NoSuchElementException">No element was found.</exception>
        public static void SetValue(this IWebDriver webdriver, string selector, string value)
        {
            By locator = By.CssSelector(selector);
            IWebElement element = webdriver.FindElement(locator);
            element.Clear();
            element.SendKeys(value);
        }

        /// <summary>
        /// Sets an element's (an input field) value to the provided text, using the given CSS selector and using SendKeys().
        /// </summary>
        /// <param name="selector">A valid CSS selector.</param>
        /// <param name="value">The text to type.</param>
        /// <param name="itemIndex">A zero-based index that determines which element to target from the CSS selector (assuming
        /// the CSS selector returns more than one element).</param>
        /// <param name="webdriver">A <see cref="IWebDriver"/> instance.</param>
        /// <exception cref="OpenQA.Selenium.NoSuchElementException">No element was found.</exception>
        public static void SetValue(this IWebDriver webdriver, string selector, string value, int itemIndex)
        {
            By locator = By.CssSelector(selector);
            IList<IWebElement> elements = webdriver.FindElements(locator);
            IWebElement element = elements[itemIndex];
            element.Clear();
            element.SendKeys(value);
        }

        /// <summary>
        /// Sets the textbox with the given CSS id to the provided value.
        /// </summary>
        /// <param name="idEndsWith">A CSS id.</param>
        /// <param name="value">The text to type.</param>
        /// <param name="webdriver">A <see cref="IWebDriver"/> instance.</param>
        /// <exception cref="OpenQA.Selenium.NoSuchElementException">No element was found.</exception>
        public static void FillTextBox(this IWebDriver webdriver, string idEndsWith, string value)
        {
            string exp = String.Format("input[id$='{0}']", idEndsWith);
            webdriver.SetValue(exp, value);
        }

        /// <summary>
        /// Sets the textarea with the given CSS id to the provided value.
        /// </summary>
        /// <param name="value">The text to set the value to.</param>
        /// <param name="idEndsWith">A CSS id.</param>
        /// <param name="webdriver">A <see cref="IWebDriver"/> instance.</param>
        /// <exception cref="OpenQA.Selenium.NoSuchElementException">No element was found.</exception>
        public static void FillTextArea(this IWebDriver webdriver, string idEndsWith, string value)
        {
            string exp = String.Format("textarea[id$='{0}']", idEndsWith);
            webdriver.SetValue(exp, value);
        }

        /// <summary>
        /// Waits the specified time in second (using a thread sleep)
        /// </summary>
        /// <param name="seconds">The number of seconds to wait (this uses TimeSpan.FromSeconds)</param>
        /// <param name="webdriver">A <see cref="IWebDriver"/> instance.</param>
        public static void Wait(this IWebDriver webdriver, double seconds)
        {
            TimeSpan time = TimeSpan.FromSeconds(seconds);
            Thread.Sleep(time);
        }

        /// <summary>
        /// Waits 2 seconds, which is *usually* the maximum time needed for all Javascript execution to finish on the page.
        /// </summary>
        /// <param name="webdriver">A <see cref="IWebDriver"/> instance.</param>
        public static void WaitForPageLoad(this IWebDriver webdriver)
        {
            webdriver.Wait(2);
        }
    }
}