namespace OpenQA.Selenium.Remote
{
    using System;
    using OpenQA.Selenium;

    /// <summary>
    /// Permite generar una captura de pantalla.
    /// </summary>
    public class ScreenShotRemoteWebDriver : RemoteWebDriver, ITakesScreenshot
    {
        #region Ctx

        /// <summary>
        /// 
        /// </summary>
        /// <param name="remoteAdress"></param>
        /// <param name="capabilities"></param>
        public ScreenShotRemoteWebDriver(Uri remoteAdress, ICapabilities capabilities)
            : base(remoteAdress, capabilities)
        {
        }

        public ScreenShotRemoteWebDriver(ICommandExecutor commandExecutor, ICapabilities desiredCapabilities)
            : base(commandExecutor, desiredCapabilities)
        {

        }

        public ScreenShotRemoteWebDriver(ICapabilities desiredCapabilities)
            : base(desiredCapabilities)
        {

        }

        public ScreenShotRemoteWebDriver(Uri remoteAddress, ICapabilities desiredCapabilities, TimeSpan commandTimeout)
            : base(remoteAddress, desiredCapabilities, commandTimeout)
        {

        }

        #endregion

        #region Methods
        /// <summary>
        /// Gets a <see cref="Screenshot"/> object representing the image of the page on the screen.
        /// </summary>
        /// <returns>A <see cref="Screenshot"/> object containing the image.</returns>
        public Screenshot GetScreenshot()
        {
            // Get the screenshot as base64.
            Response screenshotResponse = this.Execute(DriverCommand.Screenshot, null);
            string base64 = screenshotResponse.Value.ToString();

            // ... and convert it.
            return new Screenshot(base64);
        }

        #endregion
    }
}
