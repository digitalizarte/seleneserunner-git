
namespace OpenQA.Selenium.Support.Events
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using OpenQA.Selenium.Support.Events;

    public static class EventFiringWebDriverExtension
    {
        public static EventFiringWebDriver AddElementClickedHandler(this EventFiringWebDriver driver, EventHandler<WebElementEventArgs> handler)
        {
            if (driver == null)
            {
                throw new ArgumentNullException("driver");
            }

            driver.ElementClicked += handler;

            return driver;
        }

        public static EventFiringWebDriver RemoveElementClickedHandler(this EventFiringWebDriver driver, EventHandler<WebElementEventArgs> handler)
        {
            if (driver == null)
            {
                throw new ArgumentNullException("driver");
            }

            driver.ElementClicked -= handler;

            return driver;
        }

        public static EventFiringWebDriver AddElementClickingHandler(this EventFiringWebDriver driver, EventHandler<WebElementEventArgs> handler)
        {
            if (driver == null)
            {
                throw new ArgumentNullException("driver");
            }

            driver.ElementClicking += handler;

            return driver;
        }

        public static EventFiringWebDriver RemoveElementClickingHandler(this EventFiringWebDriver driver, EventHandler<WebElementEventArgs> handler)
        {
            if (driver == null)
            {
                throw new ArgumentNullException("driver");
            }

            driver.ElementClicking -= handler;

            return driver;
        }

        public static EventFiringWebDriver AddElementValueChangedHandler(this EventFiringWebDriver driver, EventHandler<WebElementEventArgs> handler)
        {
            if (driver == null)
            {
                throw new ArgumentNullException("driver");
            }

            driver.ElementValueChanged += handler;

            return driver;
        }

        public static EventFiringWebDriver RemoveElementValueChangedHandler(this EventFiringWebDriver driver, EventHandler<WebElementEventArgs> handler)
        {
            if (driver == null)
            {
                throw new ArgumentNullException("driver");
            }

            driver.ElementValueChanged -= handler;

            return driver;
        }

        public static EventFiringWebDriver AddElementValueChangingHandler(this EventFiringWebDriver driver, EventHandler<WebElementEventArgs> handler)
        {
            if (driver == null)
            {
                throw new ArgumentNullException("driver");
            }

            driver.ElementValueChanging += handler;

            return driver;
        }

        public static EventFiringWebDriver RemoveElementValueChangingHandler(this EventFiringWebDriver driver, EventHandler<WebElementEventArgs> handler)
        {
            if (driver == null)
            {
                throw new ArgumentNullException("driver");
            }

            driver.ElementValueChanging -= handler;

            return driver;
        }

        public static EventFiringWebDriver AddExceptionThrownHandler(this EventFiringWebDriver driver, EventHandler<WebDriverExceptionEventArgs> handler)
        {
            if (driver == null)
            {
                throw new ArgumentNullException("driver");
            }

            driver.ExceptionThrown += handler;

            return driver;
        }

        public static EventFiringWebDriver RemoveExceptionThrownHandler(this EventFiringWebDriver driver, EventHandler<WebDriverExceptionEventArgs> handler)
        {
            if (driver == null)
            {
                throw new ArgumentNullException("driver");
            }

            driver.ExceptionThrown -= handler;

            return driver;
        }

        public static EventFiringWebDriver AddFindElementCompletedHandler(this EventFiringWebDriver driver, EventHandler<FindElementEventArgs> handler)
        {
            if (driver == null)
            {
                throw new ArgumentNullException("driver");
            }

            driver.FindElementCompleted += handler;

            return driver;
        }

        public static EventFiringWebDriver RemoveFindElementCompletedHandler(this EventFiringWebDriver driver, EventHandler<FindElementEventArgs> handler)
        {
            if (driver == null)
            {
                throw new ArgumentNullException("driver");
            }

            driver.FindElementCompleted -= handler;

            return driver;
        }

        public static EventFiringWebDriver AddFindingElementHandler(this EventFiringWebDriver driver, EventHandler<FindElementEventArgs> handler)
        {
            if (driver == null)
            {
                throw new ArgumentNullException("driver");
            }

            driver.FindingElement += handler;

            return driver;
        }

        public static EventFiringWebDriver RemoveFindingElementHandler(this EventFiringWebDriver driver, EventHandler<FindElementEventArgs> handler)
        {
            if (driver == null)
            {
                throw new ArgumentNullException("driver");
            }

            driver.FindingElement -= handler;

            return driver;
        }

        public static EventFiringWebDriver AddNavigatedHandler(this EventFiringWebDriver driver, EventHandler<WebDriverNavigationEventArgs> handler)
        {
            if (driver == null)
            {
                throw new ArgumentNullException("driver");
            }

            driver.Navigated += handler;

            return driver;
        }

        public static EventFiringWebDriver RemoveNavigatedHandler(this EventFiringWebDriver driver, EventHandler<WebDriverNavigationEventArgs> handler)
        {
            if (driver == null)
            {
                throw new ArgumentNullException("driver");
            }

            driver.Navigated -= handler;

            return driver;
        }

        public static EventFiringWebDriver AddNavigatedBackHandler(this EventFiringWebDriver driver, EventHandler<WebDriverNavigationEventArgs> handler)
        {
            if (driver == null)
            {
                throw new ArgumentNullException("driver");
            }

            driver.NavigatedBack += handler;

            return driver;
        }

        public static EventFiringWebDriver RemoveNavigatedBackHandler(this EventFiringWebDriver driver, EventHandler<WebDriverNavigationEventArgs> handler)
        {
            if (driver == null)
            {
                throw new ArgumentNullException("driver");
            }

            driver.NavigatedBack -= handler;

            return driver;
        }

        public static EventFiringWebDriver AddNavigatedForwardHandler(this EventFiringWebDriver driver, EventHandler<WebDriverNavigationEventArgs> handler)
        {
            if (driver == null)
            {
                throw new ArgumentNullException("driver");
            }

            driver.NavigatedForward += handler;

            return driver;
        }

        public static EventFiringWebDriver RemoveNavigatedForwardHandler(this EventFiringWebDriver driver, EventHandler<WebDriverNavigationEventArgs> handler)
        {
            if (driver == null)
            {
                throw new ArgumentNullException("driver");
            }

            driver.NavigatedForward -= handler;

            return driver;
        }

        public static EventFiringWebDriver AddNavigatingHandler(this EventFiringWebDriver driver, EventHandler<WebDriverNavigationEventArgs> handler)
        {
            if (driver == null)
            {
                throw new ArgumentNullException("driver");
            }

            driver.Navigating += handler;

            return driver;
        }

        public static EventFiringWebDriver RemoveNavigatingHandler(this EventFiringWebDriver driver, EventHandler<WebDriverNavigationEventArgs> handler)
        {
            if (driver == null)
            {
                throw new ArgumentNullException("driver");
            }

            driver.Navigating -= handler;

            return driver;
        }

        public static EventFiringWebDriver AddNavigatingBackHandler(this EventFiringWebDriver driver, EventHandler<WebDriverNavigationEventArgs> handler)
        {
            if (driver == null)
            {
                throw new ArgumentNullException("driver");
            }

            driver.NavigatingBack += handler;

            return driver;
        }

        public static EventFiringWebDriver RemoveNavigatingBackHandler(this EventFiringWebDriver driver, EventHandler<WebDriverNavigationEventArgs> handler)
        {
            if (driver == null)
            {
                throw new ArgumentNullException("driver");
            }

            driver.NavigatingBack -= handler;

            return driver;
        }

        public static EventFiringWebDriver AddNavigatingForwardHandler(this EventFiringWebDriver driver, EventHandler<WebDriverNavigationEventArgs> handler)
        {
            if (driver == null)
            {
                throw new ArgumentNullException("driver");
            }

            driver.NavigatingForward += handler;

            return driver;
        }

        public static EventFiringWebDriver RemoveNavigatingForwardHandler(this EventFiringWebDriver driver, EventHandler<WebDriverNavigationEventArgs> handler)
        {
            if (driver == null)
            {
                throw new ArgumentNullException("driver");
            }

            driver.NavigatingForward -= handler;

            return driver;
        }

        public static EventFiringWebDriver AddScriptExecutedHandler(this EventFiringWebDriver driver, EventHandler<WebDriverScriptEventArgs> handler)
        {
            if (driver == null)
            {
                throw new ArgumentNullException("driver");
            }

            driver.ScriptExecuted += handler;

            return driver;
        }

        public static EventFiringWebDriver RemoveScriptExecutedHandler(this EventFiringWebDriver driver, EventHandler<WebDriverScriptEventArgs> handler)
        {
            if (driver == null)
            {
                throw new ArgumentNullException("driver");
            }

            driver.ScriptExecuted -= handler;

            return driver;
        }
    }
}
