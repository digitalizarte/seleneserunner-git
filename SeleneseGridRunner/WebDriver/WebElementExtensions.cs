using System.Reflection;
using OpenQA.Selenium.Internal;

namespace OpenQA.Selenium
{
    using OpenQA.Selenium.Remote;
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Linq;
    using System.Threading;

    public static partial class WebElementExtensions
    {
        /// <summary>
        /// Desenvuelve el elemento.
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="element"></param>
        /// <returns></returns>
        public static TEntity UnWrapStatic<TEntity>(IWebElement element)
            where TEntity : IWebElement
        {
            const string ClassName = "EventFiringWebElement";
            const string FieldName = "underlyingElement";
            TEntity el;
            // Fix            
            if (element is IWrapsElement)
            {
                el = (TEntity)(element as IWrapsElement).WrappedElement;
            }
            else if (element.GetType().Name == ClassName)
            {
                el = (TEntity)element.GetType().GetField(FieldName, BindingFlags.Instance | BindingFlags.NonPublic).GetValue(element);
            }
            else
            {
                el = (TEntity)element;
            }
            return el;
        }

        /// <summary>
        /// Desenvuelve el elemento.
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="element"></param>
        /// <returns></returns>
        public static TEntity UnWrap<TEntity>(this IWebElement element) 
            where TEntity : IWebElement
        {
            return UnWrapStatic<TEntity>(element);
        }

        /// <summary> 
        /// Busca un elemento en la pagina y le hace foco. 
        /// </summary> 
        /// <param name="driver">Driver que controla la pagina</param> 
        /// <param name="by">Selector.</param> 
        /// <returns>Devuelve el elemento al que se le realizo el foco.</returns> 
        public static void Focus(this IWebElement element)
        {
            // Busca el elemento. 
            // RemoteWebElement el = (RemoteWebElement)element;
            RemoteWebElement el = UnWrapStatic<RemoteWebElement>(element);

            // Hace foco al elemento.  
            Point hack = el.LocationOnScreenOnceScrolledIntoView;
        }

        private static readonly Random random = new Random();
        private static readonly object syncLock = new object();

        private static int RandomNext()
        {
            lock (syncLock)
            {
                return random.Next();
            }
        }

        /// <summary>
        /// Ilumina el elemento seleccionado.
        /// </summary>
        /// <param name="element"></param>
        public static void HighlightElement(this IWebElement element)
        {
            IWebDriver driver = UnWrapStatic<RemoteWebElement>(element).WrappedDriver;

            IJavaScriptExecutor js = driver.Scripts();
            try
            {
                int rnd = RandomNext();

                js.ExecuteScript(String.Format(@"var rect = arguments[0].getBoundingClientRect(),
                                               div = document.createElement('div'); 
                                               div.setAttribute('id', 'webDriverHighlightElement{0}'); 
                                               div.setAttribute('style', 'border:3px solid yellow; display:none; position:absolute; left:' + String(rect.left - 5) + 'px; top:' + String(rect.top - 5) + 'px; width:' + String( (arguments[0].offsetWidth || arguments[0].clientWidth || 0 ) + 5 ) + 'px; height:' + String( (arguments[0].offsetHeight || arguments[0].clientHeight || 0) + 5) + 'px; '); 
                                           document.body.appendChild(div);
                                           return div;", rnd), element);

                for (int i = 0; i < 2; i++)
                {
                    js.ExecuteScript(String.Format("document.getElementById('webDriverHighlightElement{0}').style.display = 'block';", rnd));
                    Thread.Sleep(50);
                    js.ExecuteScript(String.Format("document.getElementById('webDriverHighlightElement{0}').style.display = 'none';", rnd));
                    Thread.Sleep(10);
                }

                js.ExecuteScript(String.Format(@"document.body.removeChild(document.getElementById('webDriverHighlightElement{0}'));", rnd));

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
        }
    }
}
