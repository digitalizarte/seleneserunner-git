namespace OpenQA.Selenium
{
    using System;
    using System.Linq;
    using System.Collections;
    using System.Collections.Specialized;
    using System.Collections.Generic;
    using OpenQA.Selenium.Support.Events;

    /// <summary>
    /// Extensiones para facilitar la ejecución de codigo javascript.
    /// </summary>
    public static class JavascriptExtension
    {
        /// <summary>
        /// Hace un cast del driver para poder ejecutar Javascipt.
        /// </summary>
        /// <param name="driver"></param>
        /// <returns></returns>
        public static IJavaScriptExecutor Scripts(this IWebDriver driver)
        {
            if (driver == null)
            {
                throw new ArgumentNullException("driver");
            }

            return (IJavaScriptExecutor)driver;
        }

        /// <summary>
        /// Ejecuta los scripts en el driver.
        /// </summary>
        /// <param name="driver"></param>
        /// <param name="script"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public static object Execute(this IWebDriver driver, string script, params object[] args)
        {
            if (driver == null)
            {
                throw new ArgumentNullException("driver");
            }

            return (driver as IJavaScriptExecutor).ExecuteScript(script, args);
        }

        public static void RecursiveInjectErrorHandler(this IWebDriver driver)
        {
            if (driver == null)
            {
                throw new ArgumentNullException("driver");
            }

            Stack<int> path = new Stack<int>();
            // driver.RecursiveExecuteAction(path, InjectErrorHandler);
            driver.UnWrap<IWebDriver>().RecursiveExecuteAction(path, InjectErrorHandler);

        }

        const string addErrorHandlerScript = @"(function(win){ 
                                                wd = win.__wd = win.__wd || {};
                                                err = wd.error = wd.error || {};                                            
                                                err.collection = err.collection || [];                                            
                                                if(!window['onerror'] || !window.onerror['hookErrorHandler']){
                                                    err.onErrorHandlerOldFn = window.onerror || function onErrorDummyFn(){};
                                                    win.onerror = function onErrorHandler(message, url, linenumber) {
                                                            message = message || '';
                                                            url = url || '';
                                                            linenumber = linenumber || 0;
                                                            // err.collection[err.collection.length] = { message : message, url : url, linenumber : linenumber };
                                                            err.collection.push({ message : message, url : url, linenumber : linenumber });
                                                            err.onErrorHandlerOldFn.apply(window, arguments);
                                                    };
                                                    win.onerror.hookErrorHandler = true;
                                                }
                                            })(window)";

        public static void InjectErrorHandler(this IWebDriver driver)
        {
            if (driver == null)
            {
                throw new ArgumentNullException("driver");
            }

            // driver.Scripts().ExecuteScript(errorHandlerScript);
            driver.UnWrap<IWebDriver>().Scripts().ExecuteScript(addErrorHandlerScript);
        }

        public static void RecursiveRemoveErrorHandler(this IWebDriver driver)
        {
            if (driver == null)
            {
                throw new ArgumentNullException("driver");
            }
            Stack<int> path = new Stack<int>();
            // driver.RecursiveExecuteAction(path, RemoveErrorHandler);
            driver.UnWrap<IWebDriver>().RecursiveExecuteAction(path, RemoveErrorHandler);
        }

        private static void RecursiveExecuteAction(this IWebDriver driver, Stack<int> path, Action<IWebDriver> action)
        {
            if (driver == null)
            {
                throw new ArgumentNullException("driver");
            }

            if (path == null)
            {
                throw new ArgumentNullException("path");
            }

            if (action == null)
            {
                throw new ArgumentNullException("action");
            }

            // Ejecuta la acción.
            action(driver);
            // Reposiciona el frame
            driver.SwitchTo().DefaultContent();
            IList<int> paths = path.ToList();
            for (int f = 0, l = paths.Count; f < l; f++)
            {
                driver.SwitchTo().Frame(paths[f]);
            }

            // Busca los IFRAMES
            By iframe = By.TagName("IFRAME");
            IList<IWebElement> iframes = driver.FindElements(iframe);
            for (int i = 0, len = iframes.Count; i < len; i++)
            {
                path.Push(i);
                driver.RecursiveExecuteAction(path, action);
            }

            // Busca los FRAMES
            By frame = By.TagName("FRAME");
            IList<IWebElement> frames = driver.FindElements(frame);
            for (int i = 0, len = frames.Count; i < len; i++)
            {
                path.Push(i);
                driver.RecursiveExecuteAction(path, action);
            }
        }

        const string removeErrorHandlerScript = @"(function(win){ 
                                                if((wd = win['__wd']) && (err = wd.error)) {
                                                    window.onerror = err.onErrorHandlerOldFn;                                                                                    
                                                    delete err.oldErrorHandler;
                                                    delete err.collection;
                                                    delete err;
                                                    delete wd.error;
                                                }
                                            })(window)";

        public static void RemoveErrorHandler(this IWebDriver driver)
        {
            if (driver == null)
            {
                throw new ArgumentNullException("driver");
            }

            // driver.Scripts().ExecuteScript(script);
            driver.UnWrap<IWebDriver>().Scripts().ExecuteScript(removeErrorHandlerScript);
        }

        const string errorCaptureScript = @"var ret;
                                             if((wd = window['__wd']) && (err = wd.error)) {
                                                ret = err.collection;
                                             }
                                             return ret || [];";

        public static object CaptureJsErros(this IWebDriver driver)
        {
            if (driver == null)
            {
                throw new ArgumentNullException("driver");
            }

            // var ret = driver.Scripts().ExecuteScript(captureErrorsScript);
            var ret = driver.UnWrap<IWebDriver>().Scripts().ExecuteScript(errorCaptureScript);

            return ret;
        }

        public static IList<object> RecursiveCaptureJsErros(this IWebDriver driver)
        {
            if (driver == null)
            {
                throw new ArgumentNullException("driver");
            }
            List<object> errors = new List<object>();
            Action<IWebDriver> action = drv =>
            {
                errors.AddRange((IList<object>)drv.CaptureJsErros());
            };
            Stack<int> path = new Stack<int>();
            // driver.RecursiveExecuteAction(path, action);
            driver.UnWrap<IWebDriver>().RecursiveExecuteAction(path, action);
            return errors;
        }
    }
}
