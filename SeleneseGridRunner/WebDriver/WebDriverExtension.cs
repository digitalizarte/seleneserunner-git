namespace OpenQA.Selenium
{
    using NUnit.Framework;
    using OpenQA.Selenium.Remote;
    using OpenQA.Selenium.Support.UI;
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Globalization;
    using System.Linq;
    using System.Threading;
    using OpenQA.Selenium.Internal;

    /// <summary> 
    /// Extensiones de Driver 
    /// </summary> 
    public static partial class WebDriverExtensions
    {
        const string returnDocumentreadyState = "return document.readyState";
        const string complete = "complete";

        public static void HighlightElement(this IWebDriver driver, By by)
        {
            IWebElement element = driver.FindElementDeep(by);
            element.HighlightElement();
        }

        /// <summary> 
        /// Verifica que el elemento este presente o espera y vuelve a verificar. 
        /// Se se supero el tiempo de espera genera una falla en el test. 
        /// </summary> 
        /// <param name="by">Elemento a buscar.</param> 
        /// <param name="timeout">Tiempo de espera.</param> 
        public static By WaitForElementPresent(this IWebDriver driver, By by, uint timeout = 60)
        {
            uint sec = 0;
            bool ok = false;
            while (!ok && sec <= timeout)
            {
                try
                {
                    driver.FindElementDeep(by);
                    ok = true;
                }
                catch (NoSuchElementException)
                {
                    Thread.Sleep(1000);
                    sec++;
                }
            }
            if (!ok)
            {
                Assert.Fail(String.Format(CultureInfo.InvariantCulture, "timeout {0}", by));
            }
            return by;
        }

        public static By WaitForElement(this IWebDriver driver, By by, bool present = true, uint timeout = 60)
        {
            uint sec = 0;
            bool ok = false;
            while (!ok && sec <= timeout)
            {
                try
                {

                    driver.FindElementDeep(by);
                    if (present)
                    {
                        ok = true;
                    }
                }
                catch (NoSuchElementException)
                {
                    if (present)
                    {
                        Thread.Sleep(1000);
                    }
                    else
                    {
                        ok = true;
                    }
                    sec++;
                }
            }

            if (!ok)
            {
                Assert.Fail(String.Format(CultureInfo.InvariantCulture, "timeout {0}", by));
            }
            return by;
        }

        public static bool IsAlertPresent(this IWebDriver driver)
        {
            try
            {
                driver.SwitchTo().Alert();
                return true;
            }
            catch (NoAlertPresentException)
            {
                return false;
            }
        }

        public static bool IsElementPresent(this IWebDriver driver, By by)
        {
            bool ret = false;
            try
            {
                // driver.FindElement(by);
                driver.FindElementDeep(by);
                ret = true;
            }
            catch (NoSuchElementException)
            {
                ret = false;
            }
            return ret;
        }

        /// <summary> 
        /// Verifica que el elemento este visible o espera y vuelve a verificar. 
        /// Se se supero el tiempo de espera genera una falla en el test. 
        /// </summary> 
        /// <param name="by">Elemento a buscar.</param> 
        /// <param name="timeout">Tiempo de espera.</param> 
        public static By WaitForVisible(this IWebDriver driver, By by, uint timeout = 60)
        {
            uint sec = 0;
            bool visible = false;
            while (!visible && sec <= timeout)
            {
                try
                {
                    visible = driver.FindElementDeep(by).Displayed;
                }
                catch (Exception)
                {
                    Thread.Sleep(1000);
                    sec++;
                }
            }
            if (!visible)
            {
                Assert.Fail(String.Format(CultureInfo.InvariantCulture, "timeout {0}", by));
            }
            return by;
        }

        /// <summary> 
        /// Verifica que el elemento este visible o espera y vuelve a verificar. 
        /// Se se supero el tiempo de espera genera una falla en el test. 
        /// </summary> 
        /// <param name="by">Elemento a buscar.</param> 
        /// <param name="timeout">Tiempo de espera.</param> 
        public static void WaitForCondition(this IWebDriver driver, Func<bool> condition, uint timeout = 60)
        {
            uint sec = 0;
            bool ok = false;
            while (!ok && sec <= timeout)
            {
                try
                {
                    ok = condition();
                }
                catch (Exception)
                {
                    Thread.Sleep(1000);
                    sec++;
                }
            }
            if (!ok)
            {
                Assert.Fail("timeout");
            }
        }

        public static string CloseAlertAndGetItsText(this IWebDriver driver, bool acceptNextAlert = true)
        {
            try
            {
                IAlert alert = driver.SwitchTo().Alert();
                string alertText = alert.Text;
                if (acceptNextAlert)
                {
                    alert.Accept();
                }
                else
                {
                    alert.Dismiss();
                }
                return alertText;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="driver"></param>
        /// <param name="by"></param>
        /// <returns></returns>
        /// <see cref="http://darrellgrainger.blogspot.com.ar/2012/04/frames-and-webdriver.html"/>
        public static IWebElement FindElementDeep(this IWebDriver driver, By by)
        {
            Stack<int> path = new Stack<int>();
            IWebElement ret = FindElementDeep(driver, by, path);
            if (ret == null)
            {
                throw new NoSuchElementException();
            }
            return ret;
        }

        private static IWebElement FindElementDeep(IWebDriver driver, By by, Stack<int> path)
        {
            IWebElement ret = null;
            try
            {
                ret = driver.FindElement(by);
            }
            catch (NoSuchElementException)
            {
                driver.SwitchTo().DefaultContent();
                IList<int> paths = path.ToList();
                for (int f = 0, l = paths.Count; f < l; f++)
                {
                    driver.SwitchTo().Frame(paths[f]);
                }

                By iframe = By.TagName("IFRAME");
                IList<IWebElement> iframes = driver.FindElements(iframe);
                bool ok = false;
                int i = 0, len = iframes.Count;
                while (!ok && i < len)
                {
                    try
                    {
                        path.Push(i);
                        ret = FindElementDeep(driver, by, path);
                        ok = ret != null;
                    }
                    catch (NoSuchElementException)
                    {
                    }
                    path.Pop();
                    i++;
                }

                if (!ok)
                {
                    By frame = By.TagName("FRAME");
                    IList<IWebElement> frames = driver.FindElements(frame);
                    ok = false;
                    i = 0;
                    len = frames.Count;
                    while (!ok && i < len)
                    {
                        try
                        {
                            path.Push(i);
                            ret = FindElementDeep(driver, by, path);
                            ok = ret != null;
                        }
                        catch (NoSuchElementException)
                        {
                        }
                        path.Pop();
                        i++;
                    }
                }
            }
            return ret;
        }

        public static void WaitForPageToLoad(this IWebDriver driver, uint timeout = 60)
        {
            // Wait for load.
            IWait<IWebDriver> wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60));
            wait.Until(d => ((IJavaScriptExecutor)d).ExecuteScript(returnDocumentreadyState).Equals(complete));
        }

        /// <summary> 
        /// Busca un elemento en la pagina y le hace foco. 
        /// </summary> 
        /// <param name="driver">Driver que controla la pagina</param> 
        /// <param name="by">Selector.</param> 
        /// <returns>Devuelve el elemento al que se le realizo el foco.</returns> 
        public static IWebElement FindElementOnPage(this IWebDriver driver, By by)
        {
            //// Busca el elemento. 
            //RemoteWebElement element = (RemoteWebElement)driver.FindElementDeep(by);
            //// Hace foco al elemento.  
            //Point hack = element.LocationOnScreenOnceScrolledIntoView;
            //return element;

            //IWebElement element = driver.FindElementDeep(by);
            //element.Focus();
            //return element;

            return driver.Focus(by);
        }

        /// <summary>
        /// Busca un elemento en la pagina y le hace foco. 
        /// </summary>
        /// <param name="driver">Driver que controla la pagina</param> 
        /// <param name="by">Selector.</param> 
        /// <returns>Devuelve el elemento al que se le realizo el foco.</returns> 
        public static IWebElement Focus(this IWebDriver driver, By by)
        {
            IWebElement element = driver.FindElementDeep(by);
            element.Focus();
            return element;
        }

        public static IWebDriver Frame(this ITargetLocator locator, By by)
        {
            IWebDriver ret;
            using (IWebDriver driver = locator.DefaultContent())
            {
                ret = locator.Frame(driver.FindElement(by));
            };
            return ret;
        }

        public static TEntity UnWrapStatic<TEntity>(IWebDriver driver)
            where TEntity : IWebDriver
        {
            TEntity drv;
            if (driver is IWrapsDriver)
            {
                drv = (TEntity)(driver as IWrapsDriver).WrappedDriver;
            }
            else
            {
                drv = (TEntity)driver;
            }

            return drv;
        }

        public static TEntity UnWrap<TEntity>(this IWebDriver driver)
            where TEntity : IWebDriver
        {
            return UnWrapStatic<TEntity>(driver);
        }
    }
}